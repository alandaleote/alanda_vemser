class Pokemon {
    constructor( obj ){
        this.nome = obj.name;
        this.id = obj.id;
        this.height = obj.height;
        this.weight = obj.weight;
        this.pokemonImage = obj.sprites.front_default;
        this.types = [...obj.types];
        this.stats = [...obj.stats];
    }
}