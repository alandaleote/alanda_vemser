class PokeApi {
    constructor() { }

    buscarTodos() {
        let fazRequisicao = fetch(`https://pokeapi.co/api/v2/pokemon`);
        return fazRequisicao.then( resultadoEmString => resultadoEmString.json() );
    }

    buscar(id) {
        let fazRequisicao = fetch(`https://pokeapi.co/api/v2/pokemon/${id}`);
        return fazRequisicao.then( resultadoEmString => resultadoEmString.json() );
    }
}