package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import br.com.dbccompany.coworking.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/clientespacotes" )
public class ClientesPacotesController {
    @Autowired
    ClientesPacotesService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ClientesPacotes> todosClientesPacotes(){
        return service.todosClientesPacotes();
    }

    @GetMapping( value = "buscar/{id}" )
    @ResponseBody
    public ClientesPacotes buscarPorId(@PathVariable  Integer id ) {
        return service.clientePacoteEspecifico( id );
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ClientesPacotes novoClientesPacotes(@RequestBody ClientesPacotes clientesPacotes){
        return service.salvar(clientesPacotes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ClientesPacotes editarClientesPacotes(@PathVariable Integer id, @RequestBody ClientesPacotes clientesPacotes){
        return service.editar(clientesPacotes, id);
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarId(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}
