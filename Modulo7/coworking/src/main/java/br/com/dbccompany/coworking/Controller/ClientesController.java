package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/clientes" )
public class ClientesController {
    @Autowired
    ClientesService service;

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Clientes novoCliente(@RequestBody Clientes clientes){
        return service.salvar(clientes);
    }

    @GetMapping( value = "buscar/todos" )
    @ResponseBody
    public List<Clientes> todosClientes(){
        return service.todosClientes();
    }

    @GetMapping( value = "buscar/{id}" )
    @ResponseBody
    public Clientes buscarPorId( @PathVariable  Integer id ) {
        return service.clienteById( id );
    }

    @GetMapping( value = "buscar/{cpf}" )
    @ResponseBody
    public Clientes buscarPorCpf( @PathVariable  String cpf ) {
        return service.clienteByCpf( cpf );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Clientes editarCliente(@PathVariable Integer id, @RequestBody Clientes clientes){
        return service.editar(clientes, id);
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarId(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}
