package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.*;

@Entity
public class Espacos {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ")
    @GeneratedValue( generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column ( name = "ID_ESPACOS")
    private Integer id;

    @OneToMany(mappedBy = "espacos" )
    private List<Contratacao> contratacao = new ArrayList<>();

    @OneToMany( mappedBy = "espacos")
    private List<SaldoCliente> saldoCliente = new ArrayList<>();

    @OneToMany(mappedBy = "espacos")
    private List<EspacosPacotes> espacosPacotes = new ArrayList<>();

    @Column (name= "NOME", nullable = false, unique = true )
    private String nome;

    @Column ( name = "QTD_PESSOAS" )
    private Integer qtdPessoas;

    @Column ( name = "VALOR" )
    private Double valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
