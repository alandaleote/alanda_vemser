package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {
    @Autowired
    private ClientesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Clientes salvar(Clientes clientes ){
        return repository.save(clientes);
    }

    @Transactional( rollbackFor = Exception.class)
    public Clientes editar( Clientes clientes, Integer id ){
        clientes.setId(id);
        return repository.save(clientes);
    }

    public List<Clientes> todosClientes(){
        return (List<Clientes>) repository.findAll();
    }

    public Clientes clienteById( Integer id ){
        Optional<Clientes> cliente = repository.findById(id);
        return cliente.get();
    }

    public Clientes clienteByCpf( String cpf ){
        return repository.findByCpf( cpf );
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }
}
