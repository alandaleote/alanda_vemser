package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Pacotes;
import br.com.dbccompany.coworking.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PacotesService {
    @Autowired
    private PacotesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Pacotes salvar(Pacotes pacotes ){
        return repository.save(pacotes);
    }

    @Transactional( rollbackFor = Exception.class)
    public Pacotes editar( Pacotes pacotes, Integer id ){
        pacotes.setId(id);
        return repository.save(pacotes);
    }

    public List<Pacotes> todosPacotes(){
        return (List<Pacotes>) repository.findAll();
    }

    public Pacotes pacoteEspecifico( Integer id ){
        Optional<Pacotes> pacote = repository.findById(id);
        return pacote.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }
}
