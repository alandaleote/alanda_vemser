package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ClientesPacotes {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTES_PCT_SEQ", sequenceName = "CLIENTES_PCT_SEQ")
    @GeneratedValue( generator = "CLIENTES_PCT_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID_CLIENTES_PACOTES")
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "ID_CLIENTES", nullable = false )
    private Clientes clientes ;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "ID_PACOTES", nullable = false )
    private Pacotes pacotes ;

    @OneToMany(mappedBy = "clientesPacotes")
    private List<Pagamentos> pagamentos = new ArrayList<>();

    @Column( name = "QUANTIDADE", nullable = false )
    private Integer quantidade;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
