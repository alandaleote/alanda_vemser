package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.SaldoCliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, Integer> {

    List<SaldoCliente> findAll();
}
