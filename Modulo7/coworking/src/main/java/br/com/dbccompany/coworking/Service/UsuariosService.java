package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Usuarios;
import br.com.dbccompany.coworking.Repository.UsuariosRepository;
import br.com.dbccompany.coworking.Security.Criptografia;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UsuariosService {
    @Autowired
    private UsuariosRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Usuarios salvar(Usuarios usuarios ){
        String senha = usuarios.getSenha();

        if( StringUtils.isAlphanumeric(senha) && senha.length() >= 6 ) {
            usuarios.setSenha(Criptografia.criptografar(senha));
        } else {
            throw new RuntimeException("Senha inválida! Mínimo seis caractéries alfanuméricos.");
        }
        return repository.save(usuarios);
    }

    @Transactional( rollbackFor = Exception.class)
    public Usuarios editar( Usuarios usuarios, Integer id ){
        usuarios.setId(id);
        return repository.save(usuarios);
    }

    @Transactional( rollbackFor = Exception.class )
    public List<Usuarios> todosUsuarios(){
        return (List<Usuarios>) repository.findAll();
    }

    public Usuarios findById( Integer id ){
        Optional<Usuarios> usuario = repository.findById(id);
        return usuario.get();
    }

    public Usuarios findByEmail( String email ) {
        return repository.findByEmail(email);
    }

    public Usuarios findByLogin( String login ) {
        return repository.findByLogin(login);
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }

}





