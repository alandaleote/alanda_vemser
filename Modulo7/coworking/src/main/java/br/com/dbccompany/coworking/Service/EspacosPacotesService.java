package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import br.com.dbccompany.coworking.Repository.EspacosPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacosPacotesService {
    @Autowired
    private EspacosPacotesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public EspacosPacotes salvar(EspacosPacotes espacosPacotes ){
        return repository.save(espacosPacotes);
    }

    @Transactional( rollbackFor = Exception.class)
    public EspacosPacotes editar( EspacosPacotes espacosPacotes, Integer id ){
        espacosPacotes.setId(id);
        return repository.save(espacosPacotes);
    }

    public List<EspacosPacotes> todosEspacosPacotes(){
        return (List<EspacosPacotes>) repository.findAll();
    }

    public EspacosPacotes espacoPacoteEspecifico( Integer id ){
        Optional<EspacosPacotes> espacoPacote = repository.findById(id);
        return espacoPacote.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }
}
