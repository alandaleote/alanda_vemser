package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import br.com.dbccompany.coworking.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/espacospacotes" )
public class EspacosPacotesController {
    @Autowired
    EspacosPacotesService service;

    @GetMapping( value = "buscar/todos" )
    @ResponseBody
    public List<EspacosPacotes> todosEspacosPacotes(){
        return service.todosEspacosPacotes();
    }

    @GetMapping( value = "buscar/{id}" )
    @ResponseBody
    public EspacosPacotes buscarPorId(@PathVariable  Integer id ) {
        return service.espacoPacoteEspecifico( id );
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public EspacosPacotes novoEspacoPacote(@RequestBody EspacosPacotes espacosPacotes){
        return service.salvar(espacosPacotes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public EspacosPacotes editarEspacoPacote(@PathVariable Integer id, @RequestBody EspacosPacotes espacosPacotes){
        return service.editar(espacosPacotes, id);
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarId(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}
