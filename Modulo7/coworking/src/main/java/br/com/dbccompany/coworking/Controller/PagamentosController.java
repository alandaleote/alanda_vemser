package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pagamentos;
import br.com.dbccompany.coworking.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/pagamentos" )
public class PagamentosController {
    @Autowired
    PagamentosService service;

    @GetMapping( value = "buscar/todos" )
    @ResponseBody
    public List<Pagamentos> todosPagamentos(){
        return service.todosPagamentos();
    }

    @GetMapping( value = "buscar/{id}" )
    @ResponseBody
    public Pagamentos buscarPorId(@PathVariable  Integer id ) {
        return service.pacoteEspecifico( id );
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Pagamentos novoPagamento(@RequestBody Pagamentos pagamentos){
        return service.salvar(pagamentos);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pagamentos editarPagamento(@PathVariable Integer id, @RequestBody Pagamentos pagamentos){
        return service.editar(pagamentos, id);
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarId(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}
