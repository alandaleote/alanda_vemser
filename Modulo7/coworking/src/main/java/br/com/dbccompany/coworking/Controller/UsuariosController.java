package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Usuarios;
import br.com.dbccompany.coworking.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/usuarios" )
public class UsuariosController {
    @Autowired
    UsuariosService service;

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Usuarios novoUsuario(@RequestBody Usuarios usuarios){
        return service.salvar(usuarios);
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public Usuarios buscarId(@PathVariable Integer id, @RequestBody Usuarios usuarios ){
        return service.findById(id);
    }

    @GetMapping( value = "/buscar/{login}" )
    @ResponseBody
    public Usuarios buscarLogin( @PathVariable String login ){
        return service.findByLogin( login );
    }

    @GetMapping( value = "/buscar/{email}" )
    @ResponseBody
    public Usuarios buscarEmail( @PathVariable String email ){
        return service.findByEmail( email );
    }

    @GetMapping( value = "buscar/todos" )
    @ResponseBody
    public List<Usuarios> todosUsuarios(){
        return service.todosUsuarios();
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Usuarios editarUsuario(@PathVariable Integer id, @RequestBody Usuarios usuarios){
        return service.editar(usuarios, id);

    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarId(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}
