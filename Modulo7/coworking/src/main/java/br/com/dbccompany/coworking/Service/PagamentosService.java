package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Pagamentos;
import br.com.dbccompany.coworking.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentosService {
    @Autowired
    private PagamentosRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Pagamentos salvar(Pagamentos pacotes ){
        return repository.save(pacotes);
    }

    @Transactional( rollbackFor = Exception.class)
    public Pagamentos editar( Pagamentos pacotes, Integer id ){
        pacotes.setId(id);
        return repository.save(pacotes);
    }

    public List<Pagamentos> todosPagamentos(){
        return (List<Pagamentos>) repository.findAll();
    }

    public Pagamentos pacoteEspecifico( Integer id ){
        Optional<Pagamentos> pacote = repository.findById(id);
        return pacote.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }
}
