package br.com.dbccompany.coworking.Entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;


@Entity
public class SaldoCliente {

    @EmbeddedId
    private KeySaldoCliente id;

    @ManyToOne
    @MapsId("ID_CLIENTES")
    @JoinColumn( name = "ID_CLIENTES", nullable = false)
    private Clientes clientes;

    @ManyToOne
    @MapsId("ID_ESPACOS")
    @JoinColumn( name = "ID_ESPACOS", nullable = false)
    private Espacos espacos;

    @Enumerated( EnumType.STRING)
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE", nullable = false )
    private Integer quantidade;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Column( name = "VENCIMENTO", nullable = false )
    private Date vencimento;

    public KeySaldoCliente getId() {
        return id;
    }

    public void setId(KeySaldoCliente id) {
        this.id = id;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

}
