package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/espacos" )
public class EspacosController {
    @Autowired
    EspacosService service;

    @GetMapping( value = "buscar/todos" )
    @ResponseBody
    public List<Espacos> todosEspacos(){
        return service.todosEspacos();
    }

    @GetMapping( value = "buscar/{id}" )
    @ResponseBody
    public Espacos buscarPorId(@PathVariable  Integer id ) {
        return service.espacoById( id );
    }

    @GetMapping( value = "buscar/{nome}" )
    @ResponseBody
    public Espacos buscarNome(@PathVariable  String nome ) {
        return service.espacoByNome( nome );
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Espacos novoEspaco(@RequestBody Espacos espacos){
        return service.salvar(espacos);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Espacos editarEspaco(@PathVariable Integer id, @RequestBody Espacos espacos){
        return service.editar(espacos, id);
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarId(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}
