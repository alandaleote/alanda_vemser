package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import br.com.dbccompany.coworking.Repository.ClientesPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientesPacotesService {
    @Autowired
    private ClientesPacotesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public ClientesPacotes salvar(ClientesPacotes clientesPacotes ){
        return repository.save(clientesPacotes);
    }

    @Transactional( rollbackFor = Exception.class)
    public ClientesPacotes editar( ClientesPacotes clientesPacotes, Integer id ){
        clientesPacotes.setId(id);
        return repository.save(clientesPacotes);
    }

    public List<ClientesPacotes> todosClientesPacotes(){
        return (List<ClientesPacotes>) repository.findAll();
    }

    public ClientesPacotes clientePacoteEspecifico( Integer id ){
        Optional<ClientesPacotes> clientePacote = repository.findById(id);
        return clientePacote.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }
}
