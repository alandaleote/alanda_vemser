package br.com.dbccompany.coworking.Entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;


@Entity
public class Acessos {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
    @GeneratedValue ( generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column ( name = "ID_ACESSOS")
    private Integer id;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(
                    name = "ID_CLIENTES_SALDO_CLIENTES",
                    referencedColumnName = "ID_CLIENTES",
                    nullable = false
            ),
            @JoinColumn(
                    name = "ID_ESPACO_SALDO_CLIENTES",
                    referencedColumnName = "ID_ESPACOS",
                    nullable = false
            )
    })
    private SaldoCliente saldoCliente;

    @Column( name = "IS_ENTRADA")
    private Boolean isEntrada;

    @DateTimeFormat(pattern = "dd/mm/yyyy")
    @Column( name = "DATA" )
    private Date data;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
