package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.KeySaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/saldocliente" )
public class SaldoClienteController {
    @Autowired
    SaldoClienteService service;

    @GetMapping( value = "buscar/todos" )
    @ResponseBody
    public List<SaldoCliente> todosSaldoCliente(){
        return service.todosSaldoCliente();
    }

    @GetMapping( value = "buscar/{id}" )
    @ResponseBody
    public SaldoCliente buscarPorId(@PathVariable  Integer id ) {
        return service.saldoClienteEspecifico( id );
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public SaldoCliente novoSaldoCliente(@RequestBody SaldoCliente saldoCliente){
        return service.salvar(saldoCliente);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public SaldoCliente editarSaldoCliente(@PathVariable KeySaldoCliente id, @RequestBody SaldoCliente saldoCliente){
        return service.editar(saldoCliente, id);
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarId(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}
