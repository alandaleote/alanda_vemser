package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/acessos" )
public class AcessosController {
    @Autowired
    AcessosService service;

    @GetMapping( value = "buscar/todos" )
    @ResponseBody
    public List<Acessos> todosAcessos(){
        return service.todosAcessos();
    }

    @GetMapping( value = "buscar/{id}" )
    @ResponseBody
    public Acessos buscarPorId(@PathVariable  Integer id ) {
        return service.acessoById( id );
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Acessos novoAcesso(@RequestBody Acessos acessos){
        return service.salvar(acessos);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Acessos editarAcesso(@PathVariable Integer id, @RequestBody Acessos acessos){
        return service.editar(acessos, id);

    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarId(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}
