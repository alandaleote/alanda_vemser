package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AcessosService {
    @Autowired
    private AcessosRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Acessos salvar(Acessos acessos ){
        return repository.save(acessos);
    }

    @Transactional( rollbackFor = Exception.class)
    public Acessos editar( Acessos acessos, Integer id ){
        acessos.setId(id);
        return repository.save(acessos);
    }

    public List<Acessos> todosAcessos(){
        return (List<Acessos>) repository.findAll();
    }

    public Acessos acessoById( Integer id ){
        Optional<Acessos> acesso = repository.findById(id);
        return acesso.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }
}
