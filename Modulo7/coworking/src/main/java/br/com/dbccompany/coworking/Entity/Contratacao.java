package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Contratacao {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID_CONTRATACAO")
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "ID_ESPACOS", nullable = false )
    private Espacos espacos ;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "ID_CLIENTES", nullable = false )
    private Clientes clientes ;

    @OneToMany(mappedBy = "contratacao")
    private List<Pagamentos> pagamentos = new ArrayList<>();

    @Enumerated( EnumType.STRING)
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE", nullable = false )
    private Integer quantidade;

    @Column( name = "DESCONTO")
    private Double desconto;

    @Column( name = "PRAZO", nullable = false )
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
