package br.com.dbccompany.coworking.Entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Clientes {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
    @GeneratedValue ( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column ( name = "ID_CLIENTES")
    private Integer id;

    @Column (name= "NOME", nullable = false)
    private String nome;

    @Column (name = "CPF", nullable = false, unique = true, length = 14)
    private String cpf;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Column ( name = "DATA_NASCIMENTO", nullable = false )
    private Date dataNascimento;

    @OneToMany( mappedBy = "clientes" )
    private List<Contato> contatos = new ArrayList<>();

    @OneToMany( mappedBy = "clientes" )
    private List<Contratacao> contratacoes = new ArrayList<>();

    @OneToMany( mappedBy = "clientes" )
    private List<ClientesPacotes> clientesPacotes = new ArrayList<>();

    @OneToMany( mappedBy = "clientes")
    private List<SaldoCliente> saldoClientes = new ArrayList<>();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<SaldoCliente> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldoCliente> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }
}
