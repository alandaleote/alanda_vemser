package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/tipocontato" )
public class TipoContatoController {
    @Autowired
    TipoContatoService service;

    @GetMapping( value = "buscar/todos" )
    @ResponseBody
    public List<TipoContato> todosTipoContato(){
        return service.todosTipoContato();
    }

    @GetMapping( value = "buscar/{id}" )
    @ResponseBody
    public TipoContato buscarPorId(@PathVariable  Integer id ) {
        return service.tipoContatoEspecifico( id );
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public TipoContato novoTipoContato(@RequestBody TipoContato tipoContato){
        return service.salvar(tipoContato);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TipoContato editarTipoContato(@PathVariable Integer id, @RequestBody TipoContato tipoContato){
        return service.editar(tipoContato, id);
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarId(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}
