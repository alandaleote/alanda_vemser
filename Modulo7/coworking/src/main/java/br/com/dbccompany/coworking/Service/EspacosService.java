package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacosService {
    @Autowired
    private EspacosRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Espacos salvar(Espacos espacos ){
        return repository.save(espacos);
    }

    @Transactional( rollbackFor = Exception.class)
    public Espacos editar( Espacos espacos, Integer id ){
        espacos.setId(id);
        return repository.save(espacos);
    }

    public List<Espacos> todosEspacos(){
        return (List<Espacos>) repository.findAll();
    }

    public Espacos espacoById( Integer id ){
        Optional<Espacos> espaco = repository.findById(id);
        return espaco.get();
    }

    public Espacos espacoByNome(String nome ) {
        return repository.findByNome( nome );
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }
}
