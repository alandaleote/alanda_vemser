package br.com.dbccompany.coworking.Entity;

public enum TipoPagamento {
    Debito, Credito, Dinheiro, Transferencia
}
