package br.com.dbccompany.coworking.Entity;

        import javax.persistence.Column;
        import javax.persistence.Embeddable;
        import java.io.Serializable;

@Embeddable
public class KeySaldoCliente implements Serializable {

    @Column( name = "ID_CLIENTES" )
    private Integer idClientes;

    @Column( name = "ID_ESPACOS" )
    private Integer idEspacos;

    public KeySaldoCliente() {}

    public KeySaldoCliente( Integer idClientes, Integer idEspacos ) {
        this.idClientes = idClientes;
        this.idEspacos = idEspacos;
    }

    public Integer getIdClientes() {
        return idClientes;
    }

    public void setIdClientes(Integer idClientes) {
        this.idClientes = idClientes;
    }

    public Integer getIdEspacos() {
        return idEspacos;
    }

    public void setIdEspacos(Integer idEspacos) {
        this.idEspacos = idEspacos;
    }
}