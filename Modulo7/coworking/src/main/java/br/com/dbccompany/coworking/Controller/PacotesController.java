package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pacotes;
import br.com.dbccompany.coworking.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/pacotes" )
public class PacotesController {
    @Autowired
    PacotesService service;

    @GetMapping( value = "buscar/todos" )
    @ResponseBody
    public List<Pacotes> todosPacotes(){
        return service.todosPacotes();
    }

    @GetMapping( value = "buscar/{id}" )
    @ResponseBody
    public Pacotes buscarPorId(@PathVariable  Integer id ) {
        return service.pacoteEspecifico( id );
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Pacotes novoPacote(@RequestBody Pacotes pacotes){
        return service.salvar(pacotes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pacotes editarPacote(@PathVariable Integer id, @RequestBody Pacotes pacotes){
        return service.editar(pacotes, id);
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarId(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}
