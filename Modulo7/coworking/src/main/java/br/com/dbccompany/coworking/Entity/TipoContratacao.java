package br.com.dbccompany.coworking.Entity;

public enum TipoContratacao {
    Minutos, Horas, Turnos, Diarias, Semanas, Meses
}
