package br.com.dbccompany.coworking.Security;

import java.math.BigInteger;
import java.security.MessageDigest;

public class Criptografia {

    public static  String criptografar( String senha ){
        String criptoSenha = "";
        MessageDigest md;
        try{
            md = MessageDigest.getInstance("MD5");
            BigInteger hash = new BigInteger( 1, md.digest(senha.getBytes()));
            criptoSenha = hash.toString(16);
        }
        catch (Exception e){
            throw new RuntimeException(e);
        }

        return criptoSenha;
    }
}