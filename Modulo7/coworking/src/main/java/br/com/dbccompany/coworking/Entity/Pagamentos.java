package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class Pagamentos {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
    @GeneratedValue( generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID_PAGAMENTOS")
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "ID_CLIENTES_PACOTES" )
    private ClientesPacotes clientesPacotes;

    @ManyToOne
    @JoinColumn( name = "ID_CONTRATACAO" )
    private Contratacao contratacao;

    @Enumerated( EnumType.STRING)
    private TipoPagamento tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesPacotes getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotes clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
