package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class EspacosPacotes {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACOS_PCT_SEQ", sequenceName = "ESPACOS_PCT_SEQ")
    @GeneratedValue( generator = "ESPACOS_PCT_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID_ESPACOS_PACOTES")
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "ID_ESPACOS" )
    private Espacos espacos;

    @ManyToOne
    @JoinColumn( name = "ID_PACOTES" )
    private Pacotes pacotes;

    @Enumerated( EnumType.STRING)
    @Column( name = "TIPO_CONTATACAO")
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE", nullable = false )
    private Integer quantidade;

    @Column( name = "PRAZO", nullable = false )
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
