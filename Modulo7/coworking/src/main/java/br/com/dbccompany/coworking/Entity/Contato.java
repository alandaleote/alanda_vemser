package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class Contato {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue( generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( name = "VALOR")
    private String valor;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "ID_TIPO_CONTATO")
    private TipoContato tipoContato;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_CLIENTE", nullable = false )
    private Clientes clientes;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

}
