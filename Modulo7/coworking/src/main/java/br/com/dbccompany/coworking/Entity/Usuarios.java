package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class Usuarios {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "USUARIOS_SEQ", sequenceName = "USUARIOS_SEQ")
    @GeneratedValue ( generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column (name = "ID_USUARIOS")
    private Integer id;

    @Column ( name = "NOME", nullable = false )
    private String nome;

    @Column ( name = "EMAIL",unique = true, nullable = false )
    private String email;

    @Column ( name = "LOGIN", unique = true, nullable = false )
    private String login;

    @Column ( name = "SENHA", nullable = false)
    private String senha;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
