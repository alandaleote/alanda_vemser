package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;

@Entity
public class Agencia {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "AGENCIA_SEQ", sequenceName = "AGENCIA_SEQ")
    @GeneratedValue(generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name="ID_AGENCIA", nullable = false)
    private Integer id;

    @Column (name= "NOME", nullable = false)
    private String nome;

    @ManyToOne
    @JoinColumn(name = "ID_BANCO", nullable = false)
    private Banco banco;

    @ManyToOne
    @JoinColumn(name = "ID_CIDADE", nullable = false)
    private Cidade cidade;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }
}
