package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;

@Entity
public class Movimentacao {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "MOVIMENTACAO_SEQ", sequenceName = "MOVIMENTACAO_SEQ")
    @GeneratedValue( generator = "MOVIMENTACAO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name="ID_MOVIMENTACAO")
    private Integer id;

    @Column (name= "VALOR", nullable = false)
    private Double valor;

    @ManyToOne
    @JoinColumn (name= "ID_CONTA")
    private Conta conta;

    @Enumerated( EnumType.STRING)
    private TipoMovimentacao tipoMovimentacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public TipoMovimentacao getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(TipoMovimentacao tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }
}

