package br.com.dbccompany.bancoDigital.Controller;

import br.com.dbccompany.bancoDigital.Entity.TipoConta;
import br.com.dbccompany.bancoDigital.Service.TipoContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/tipoconta" )
public class TipoContaController {

    @Autowired
    TipoContaService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<TipoConta> todosTipoContas(){
        return service.todosTipoConta();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public TipoConta novoTipoConta(@RequestBody TipoConta tipoconta){
        return service.salvar(tipoconta);

    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TipoConta editarTipoConta(@PathVariable Integer id, @RequestBody TipoConta tipoconta){
        return service.editar(tipoconta, id);

    }
}


