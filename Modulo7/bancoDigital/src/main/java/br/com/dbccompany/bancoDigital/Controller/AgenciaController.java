package br.com.dbccompany.bancoDigital.Controller;

import br.com.dbccompany.bancoDigital.Entity.Agencia;
import br.com.dbccompany.bancoDigital.Service.AgenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/agencia" )
public class AgenciaController {
    @Autowired
    AgenciaService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Agencia> todasAgencias(){
        return service.todasAgencias();
    }

    @PostMapping( value = "/nova" )
    @ResponseBody
    public Agencia novaAgencia(@RequestBody Agencia agencia){
        return service.salvar(agencia);

    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Agencia editarAgencia(@PathVariable Integer id, @RequestBody Agencia agencia){
        return service.editar(agencia, id);

    }
}
