package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Cidade {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CIDADE_SEQ", sequenceName = "CIDADE_SEQ")
    @GeneratedValue( generator = "CIDADE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name="ID_CIDADE", nullable = false)
    private Integer id;

    @Column (name= "NOME", nullable = false)
    private String nome;

    @ManyToOne
    @JoinColumn( name = "ID_ESTADO")
    private Estado estado;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CLIENTE_CIDADE",
            joinColumns = {@JoinColumn( name = "ID_CLIENTE")})
    private List<Cliente> cliente = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public List<Cliente> getCliente() {
        return cliente;
    }

    public void setCliente(List<Cliente> cliente) {
        this.cliente = cliente;
    }
}
