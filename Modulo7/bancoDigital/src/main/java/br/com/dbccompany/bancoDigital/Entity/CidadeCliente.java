package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;

@Embeddable
public class CidadeCliente {
    @Column( name = "ID_CIDADE")
    private Integer idCidade;

    @Column (name= "ID_CLIENTE")
    private Integer idCliente;

    public CidadeCliente() {}

    public CidadeCliente(Integer idCidade, Integer idCliente) {
        this.idCidade = idCidade;
        this.idCliente = idCliente;
    }
}