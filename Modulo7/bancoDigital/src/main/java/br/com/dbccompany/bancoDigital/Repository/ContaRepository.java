package br.com.dbccompany.bancoDigital.Repository;

import br.com.dbccompany.bancoDigital.Entity.Cliente;
import br.com.dbccompany.bancoDigital.Entity.Conta;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContaRepository extends CrudRepository<Conta, Integer>  {

    Conta findBySaldo( Double saldo );
    List<Conta> findAll();
}
