package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;
import java.util.*;

@Entity
public class Conta {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTA_SEQ", sequenceName = "CONTA_SEQ")
    @GeneratedValue( generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name="ID_CONTA")
    private Integer id;

    @ManyToOne
    @JoinColumn (name= "ID_AGENCIA")
    private Agencia agencia;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "ID_TIPO_CONTA")
    private TipoConta tipoConta;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CLIENTE_CONTA",
            joinColumns = {@JoinColumn( name = "ID_CLIENTE")})
    private List<Cliente> cliente = new ArrayList<>();

    @Column (name= "SALDO")
    private Double saldo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Cliente> getCliente() {
        return cliente;
    }

    public void setCliente(List<Cliente> cliente) {
        this.cliente = cliente;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public TipoConta getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoConta tipoConta) {
        this.tipoConta = tipoConta;
    }
}

