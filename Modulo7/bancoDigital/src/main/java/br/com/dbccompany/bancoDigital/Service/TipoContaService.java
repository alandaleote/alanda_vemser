package br.com.dbccompany.bancoDigital.Service;

import br.com.dbccompany.bancoDigital.Entity.TipoConta;
import br.com.dbccompany.bancoDigital.Repository.TipoContaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TipoContaService {

    @Autowired
    private TipoContaRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public TipoConta salvar(TipoConta tipoconta) {

        return repository.save(tipoconta);
    }

    @Transactional(rollbackFor = Exception.class)
    public TipoConta editar(TipoConta tipoconta, Integer id) {
        tipoconta.setId(id);
        return repository.save(tipoconta);
    }

    public List<TipoConta> todosTipoConta() {

        return (List<TipoConta>) repository.findAll();
    }

    public TipoConta tipoContaEspecifico(Integer id) {
        Optional<TipoConta> tipoconta = repository.findById(id);
        return tipoconta.get();
    }

}
