package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;
import java.util.*;


@Entity
public class Cliente {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue ( generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column ( name = "ID_CLIENTE")
    private Integer id;

    @Column (name= "NOME", nullable = false)
    private String nome;

    @Column (name = "CPF", nullable = false, unique = true, length = 14)
    private String cpf;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CLIENTE_CIDADE",
            joinColumns = {@JoinColumn( name = "ID_CLIENTE")},
            inverseJoinColumns = { @JoinColumn( name = "ID_CIDADE")})
    private List<Cidade> cidade = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CLIENTE_CONTA",
            joinColumns = {@JoinColumn( name = "ID_CLIENTE")},
            inverseJoinColumns = { @JoinColumn( name = "ID_CONTA")})
    private List<Conta> conta = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public List<Cidade> getCidade() {
        return cidade;
    }

    public void setCidade(List<Cidade> cidade) {
        this.cidade = cidade;
    }

    public List<Conta> getConta() {
        return conta;
    }

    public void setConta(List<Conta> conta) {
        this.conta = conta;
    }
}
