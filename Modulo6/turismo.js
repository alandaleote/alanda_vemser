

db.turismo.insert([
    { nome: "Lajeado", 
    fundacao: "26 janeiro de 1891", 
    distancia: 112,
    pontosTuristicos: [ { descricao: "Jardim Botânico de Lajeado", entrada: 0.00, funcionamento: "De sugunda a sexta das 8h às 11:30 e das 13:30 às 17:30, sábados, domigos e feriados das 8h às 12h e das 14h às 18h." }, 
        { descricao: "Parque Histórico Municipal", entrada: 0.00, funcionamento: "De sugunda a sexta das 8h às 8h, sábados, domigos e feriados das 9h às 19h" },
        { descricao: "Parque dos Dick", entrada: 0.00, funcionamento: "aberto 24hs." } ] }, 
    { nome: "Garibaldi", 
    fundacao: "31 de outubro de 1900", 
    distancia: 109, 
    pontosTuristicos: [ { descricao: "Museu Municipal", entrada: 0.00 , funcionamento: "De terça a sexta das 09h às 17h, sábados, domingos e feriados das 10h às 16h" },
        { descricao: "Vinícola Peterlongo", entrada: "sob consulta - (54) 98119-6788" , funcionamento: "Das 8h às 17h30, Domingos e Feriados: 08h às 17h" },
        { descricao: "Convento das Irmãs de São José", entrada: 0.00, funcionamento: "Das 8h às 18h." } ] },
    { nome: "Gramado", 
    fundacao: "15 de dezembro de 1954", 
    distancia: 105, 
    pontosTuristicos: [ {
        descricao: "Parque Terra Mágica Floribal", entrada: 90.00, funcionamento: "Todos os dias das 9h às 17:30" },
        {descricao: "Hollywood Dream Cars - Museu do Automóvel", entrada: 60.00, funcionamento: "Todos os dias das 8h às 18:30"},
        {descricao: "Mini Mundo", entrada: 42.00, funcionamento: "Todos os dias das 9h às 17h"},
        {descricao: "Museu Medieval Castelo Saint George", entrada: 25.00, funcionamento: "Todos os dias das 10h às 11:40 e das 13:30 às 17:40"},
        {descricao: "Mundo de Chocolate de Gramado", entrada: 48.00, funcionamento: "Todos os dias das 9:30 às 20:30"},
        {descricao: "Snowland", entrada: 151.00, funcionamento: "Todos os dias das 9h às 17h, exceto quartas-feiras."} ] },
    {nome: "Porto Alegre", 
    fundacao: "26 de março de 1772", 
    distancia: 0, 
    pontosTuristicos: [{ descricao: "City tour Centro Histórico", entrada: 30.00, funcionamento: "De terça a domingo das 9h às 16h, partida de hora em hora."},
        {descricao: "Fundação Iberê Camargo", entrada: 0.00, funcionamento: "De quarta a domingo das 14h às 18:30."},
        {descricao: "Museu de Arte do Rio Grande do Sul – MARGS", entrada: 0.00, funcionamento: "De terça a domingo, das 10h às 19h."},
        {descricao: "Museu de Ciências e Tecnologia da PUCRS",  entrada: 40.00, funcionamento:"De terça a quinta das 9h às 17h. Sexta das 9h às 21h. Sábado, domingo e feriados das 10h às 18h."},
        {descricao: "Varig Experience – Boulevard Laçador", entrada: "Visitação externa gratuita", funcionamento: "Diariamente das 8h às 23h."} ] },
    {nome: "Passo Fundo", 
    fundacao: "28 de janeiro de 1857", 
    distancia: 290, 
    pontosTuristicos: [{ descricao: "Museu de Artes Visuais Ruth Schneider", entrada: 0.00, funcionamento: "De terça a sexta das 9h às 17:30, Sábado e domingo das 13:30 às 17:30"}, 
        {descricao: "Catedral Nossa Senhora Aparecida ", entrada: 0.00, funcionamento:"Segunda a Sexta 8h30 às 12h e 13h30 às 18h, Sábados 8h às 12h"},
        {descricao: "Passo Fundo Shopping", entrada: 0.00, funcionamento: "Todos os dias das 10h às 22h."},
        {descricao: "Ginásio do Capingui", entrada: "sob consulta - 54 99636 2305", funcionamento: "verificar datas das partidas."} ] },
    {nome: "Santana do Livramento", 
    fundacao: "30 de julho de 1823", 
    distancia: 418, 
    pontosTuristicos: [{ descricao: "Praça Internacional", entrada: 0.00, funcionamento: "Todos os dias 24hs."},
        {descricao: "Lago Batuva", entrada: 0.00, funcionamento: "Todos os dias 24hs."},
        {descricao: "Amsterland",  entrada: 50.00, funcionamento: "De terça a sexta das 14h às 21h, Sábado e domingo das 11h às 21h" } ] }
    ] )


    db.turismo.updateOne({ "nome" : "Porto Alegre"}, {$set: { "populacao" : "1,409 milhão"}})

    db.turismo.updateOne({ "nome" : "Porto Alegre"}, {$unset: { "distancia" : 0} })

    db.turismo.updateOne({"nome" : "Passo Fundo"}, {$set: { "distancia" : "108" }})

    db.turismo.remove({ "nome" : "Lajeado" })

    db.turismo.find( { $and:[ {"distancia" : { $gte: 100 } }, { "pontosTuristicos.entrada" : {$lte : 50} } ] } ).pretty()

   
