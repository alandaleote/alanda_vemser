package br.com.dbccompany.Lotr.Entity;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@MappedSuperclass
@Inheritance( strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name="PERSONAGEM")
@SequenceGenerator( allocationSize = 1, name = "PERSONAGEM_SEQ",sequenceName = "PERSONAGEM_SEQ")
public class PersonagemEntity {
	
	@Id
	@GeneratedValue( generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name="ID_PERSONAGEM")
	private Integer id;
	
	@Column(name="NOME", nullable = false)
	private String nome;
	
	@Enumerated( EnumType.STRING)
	private Status status;
	
	private Double vida;
	
	@Column(name="QTD_DANO_SOFRIDO")
	private Double qtdDanoSofrido;
	
	private Integer experiencia;
	
	@Column(name="QTD_XP_POR_ATAQUE")
	private Integer qtdXpPorAtaque;
	
	@Enumerated( EnumType.STRING)
	private Tipo tipo;

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Double getVida() {
		return vida;
	}

	public void setVida(Double vida) {
		this.vida = vida;
	}

	public Double getQtdDanoSofrido() {
		return qtdDanoSofrido;
	}

	public void setQtdDanoSofrido(Double qtdDanoSofrido) {
		this.qtdDanoSofrido = qtdDanoSofrido;
	}

	public Integer getExperiencia() {
		return experiencia;
	}

	public void setExperiencia(Integer experiencia) {
		this.experiencia = experiencia;
	}

	public Integer getQtdXpPorAtaque() {
		return qtdXpPorAtaque;
	}

	public void setQtdXpPorAtaque(Integer qtdXpPorAtaque) {
		this.qtdXpPorAtaque = qtdXpPorAtaque;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
}
