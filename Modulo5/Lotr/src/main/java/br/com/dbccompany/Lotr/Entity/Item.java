package br.com.dbccompany.Lotr.Entity;

import javax.persistence.*;

@Entity
@Table(name="ITEM")
@SequenceGenerator( allocationSize = 1, name = "ITEM_SEQ",sequenceName = "ITEM_SEQ")

public class Item {
	
	@Id
	@GeneratedValue( generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name="ID_ITEM", nullable = false)
	private Integer id;
	
	@Column(name="DESCRICAO", nullable = false)
	private String descricao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
