package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="EMISSOR")
@SequenceGenerator( allocationSize = 1, name = "EMISSOR_SEQ", sequenceName = "EMISSOR_SEQ")

public class EmissorEntity {
	@Id
	@GeneratedValue( generator = "EMISSOR_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name="ID_EMISSOR", nullable = false)
	private Integer id;
	
	@Column(name="NOME", nullable = false)
	private String nome;
	
	@Column (name="TAXA", nullable = false)
	private double taxa;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getTaxa() {
		return taxa;
	}

	public void setTaxa(double taxa) {
		this.taxa = taxa;
	}
	

}
