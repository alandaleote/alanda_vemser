package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="LOJA")
@SequenceGenerator( allocationSize = 1, name = "LOJA_SEQ",sequenceName = "LOJA_SEQ")
public class LojaEntity {

	@Id
	@GeneratedValue( generator = "LOJA_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name="ID_LOJA")
	private Integer id;
	
	@Column(name="NOME", nullable = false)
	private String nome;
	
	@ManyToMany( mappedBy = "loja" )
	private List<Loja_Credenciador> Loja_Credenciador = new ArrayList<>();
	
	
	public List<Loja_Credenciador> getLoja_Credenciador() {
		return Loja_Credenciador;
	}

	public void setLoja_Credenciador(List<Loja_Credenciador> loja_Credenciador) {
		Loja_Credenciador = loja_Credenciador;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	
}