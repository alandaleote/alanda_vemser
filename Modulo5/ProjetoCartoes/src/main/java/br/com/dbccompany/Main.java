package br.com.dbccompany;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.dbccompany.Cartoes.Entity.BandeiraEntity;
import br.com.dbccompany.Cartoes.Entity.CartaoEntity;
import br.com.dbccompany.Cartoes.Entity.ClienteEntity;
import br.com.dbccompany.Cartoes.Entity.CredenciadorEntity;
import br.com.dbccompany.Cartoes.Entity.EmissorEntity;
import br.com.dbccompany.Cartoes.Entity.HibernateUtil;
import br.com.dbccompany.Cartoes.Entity.LancamentoEntity;
import br.com.dbccompany.Cartoes.Entity.LojaEntity;
import br.com.dbccompany.Cartoes.Entity.Loja_Credenciador;


public class Main {
	
	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			LojaEntity loja1 = new LojaEntity();
			LojaEntity loja2 = new LojaEntity();
			LojaEntity loja3 = new LojaEntity();
			loja1.setNome("Tok & Stok");
			loja2.setNome("Zara Home");
			loja3.setNome("Camicado");
			
			List<LojaEntity> listaLojas = new ArrayList<>();
			listaLojas.add(loja1);
			listaLojas.add(loja2);
			listaLojas.add(loja3);

			CredenciadorEntity cred1 = new CredenciadorEntity();
			CredenciadorEntity cred2 = new CredenciadorEntity();
			CredenciadorEntity cred3 = new CredenciadorEntity();
			cred1.setNome("Cred 1");
			cred2.setNome("Cred 2");
			cred3.setNome("Cred 3");

			List<CredenciadorEntity> listaCred = new ArrayList<>();
			listaCred.add(cred1);
			listaCred.add(cred2);
			listaCred.add(cred3);
			
			Loja_Credenciador loja_Cred = new Loja_Credenciador();
			loja_Cred.setLoja(listaLojas);
			loja_Cred.setCredenciador(listaCred);
			loja_Cred.setTaxa(0.1);
			
			ClienteEntity cliente1 = new ClienteEntity();
			ClienteEntity cliente2 = new ClienteEntity();
			cliente1.setNome("Pedro");
			cliente2.setNome("Julia");
	
			EmissorEntity emissor1 = new EmissorEntity();
			emissor1.setNome("Itau");
			emissor1.setTaxa(0.07);
			EmissorEntity emissor2 = new EmissorEntity();
			emissor2.setNome("Bradesco");
			emissor2.setTaxa(0.06);

			BandeiraEntity bandeira1 = new BandeiraEntity();
			bandeira1.setNome("Visa");
			bandeira1.setTaxa(0.05);
			BandeiraEntity bandeira2 = new BandeiraEntity();
			bandeira2.setNome("MasterCard");
			bandeira2.setTaxa(0.05);
			BandeiraEntity bandeira3 = new BandeiraEntity();
			bandeira3.setNome("Elo");
			bandeira3.setTaxa(0.03);
	
			CartaoEntity cartao1 = new CartaoEntity();
			cartao1.setChip(true);
			cartao1.setCliente(cliente1);
			cartao1.setBandeira(bandeira1);
			cartao1.setEmissor(emissor2);
			cartao1.setVencimento(new Date(2020, 2, 2));
			
			CartaoEntity cartao2 = new CartaoEntity();
			cartao2.setChip(true);
			cartao2.setCliente(cliente2);
			cartao2.setBandeira(bandeira3);
			cartao2.setEmissor(emissor1);
			cartao2.setVencimento(new Date(2020, 2, 5));

			LancamentoEntity lancamento1 = new LancamentoEntity();
			lancamento1.setCartao(cartao1);
			lancamento1.setLoja(loja3);
			lancamento1.setEmissor(emissor2);
			lancamento1.setDescricao("compras");
			lancamento1.setValor(120.50);
			lancamento1.setData_compra(new Date(2020, 1, 15));
			
			session.save(loja1);
			session.save(loja2);
			session.save(loja3);
			session.save(cred1);
			session.save(cred2);
			session.save(cred3);
			session.save(loja_Cred);
			session.save(cliente1);
			session.save(cliente2);
			session.save(emissor1);
			session.save(emissor2);
			session.save(bandeira1);
			session.save(bandeira2);
			session.save(bandeira3);
			session.save(cartao1);
			session.save(cartao2);
			session.save(lancamento1);
			

			transaction.commit();
		}catch (Exception e) {
			if( transaction != null ) {
				transaction.rollback();
			}
			System.exit(1);
		}finally {
			System.exit(0);
		}
	}

}

