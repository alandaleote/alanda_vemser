package br.com.dbccompany.Cartoes.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name = "LANCAMENTO" )
@SequenceGenerator( allocationSize = 1, name = "LANCAMENTO_SEQ", sequenceName  = "LANCAMENTO_SEQ")

public class LancamentoEntity {
	@Id
	@GeneratedValue( generator = "LANCAMENTO_SEQ", strategy = GenerationType.SEQUENCE )
	@Column( name = "ID_LANCAMENTO" )
	private Integer id;

	@ManyToOne
	@JoinColumn( name = "FK_ID_CARTAO")
	private CartaoEntity cartao;
	
	@ManyToOne
	@JoinColumn( name = "FK_ID_LOJA")
	private LojaEntity loja;
	
	@ManyToOne
	@JoinColumn( name = "FK_ID_EMISSOR")
	private EmissorEntity emissor;
	
	private String descricao;
	
	private double valor;
	
	private Date data_compra;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CartaoEntity getCartao() {
		return cartao;
	}

	public void setCartao(CartaoEntity cartao) {
		this.cartao = cartao;
	}

	public LojaEntity getLoja() {
		return loja;
	}

	public void setLoja(LojaEntity loja) {
		this.loja = loja;
	}

	public EmissorEntity getEmissor() {
		return emissor;
	}

	public void setEmissor(EmissorEntity emissor) {
		this.emissor = emissor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Date getData_compra() {
		return data_compra;
	}

	public void setData_compra(Date data_compra) {
		this.data_compra = data_compra;
	}

	
	
	
}
