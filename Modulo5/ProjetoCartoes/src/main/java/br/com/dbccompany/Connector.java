package br.com.dbccompany;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/* Arquivo de conexão com o banco de dados oracle, verifica se a conexão é válida. 
 * Caso não possua conexão válida, cria uma classe com o drive de conexão, seta os dados e executa um get.*/

public class Connector {
	
	private static Connection conn;
	
	public static Connection connect() {
		try {
			if(conn != null && conn.isValid(10)) {
				return conn;
			}
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:49161:XE", "system", "oracle");
			
		}catch (ClassNotFoundException | SQLException ex) {
			Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO DE CONEXÃO", ex);
		}
		return conn;
	}
	

}
