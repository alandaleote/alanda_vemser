import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/footer.css';

export default class Footer extends Component {
    render() {
      return (
        <React.Fragment>
        <footer className='main-footer'>
            <div>
                <nav>
                    <ul className='clearfix'>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/react-mirror">React-Mirror</Link>
                        </li>
                        <li>
                            <Link to="/js-flix">JS-Flix</Link>
                        </li>
                    </ul>
                </nav>
            </div>
        </footer>
        </React.Fragment>
      )
    }
  }

