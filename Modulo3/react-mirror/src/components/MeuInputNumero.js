import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class MeuInputNumero extends Component {

  perderFoco = evt => {
    const { obrigatorio, atualizarValor } = this.props
    const nota = evt.target.value
    const erro = obrigatorio && !nota
    atualizarValor( { nota, erro } )
  }

  render(){
    const { placeholder, exibir, campoMensagem, exibirErro } = this.props
    return exibir ? (
      <React.Fragment>
        <div>
          { campoMensagem && <span>{ campoMensagem }  </span> }
          { <input type="number" placeholder={ placeholder } className={ exibirErro ? 'erro' : ''} onBlur={ this.perderFoco }/> }
          { exibirErro && <span className="mensagem-erro"> *Obrigatorio</span> }
        </div>
        
      </React.Fragment>
    ) : null
  }
}

MeuInputNumero.propTypes = {
  placeholder: PropTypes.string,
  exibir: PropTypes.bool,
  exibirErro: PropTypes.bool,
  campoMensagem: PropTypes.string,
  obrigatorio: PropTypes.bool,
}

MeuInputNumero.defaultProps = {
  obrigatorio: false,
}
