import React from 'react';

export default ( props ) =>
    <div className='msg-box'>
        {props.children}
    </div>