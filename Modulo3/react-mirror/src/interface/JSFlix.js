import React, { Component } from 'react';
import '../css/App.css';
import '../css/footer.css';
import ListaSeries from '../models/listaSeries';
import Footer from '../components/footer';
import Card from '../components/card'

class JSFlix extends Component {
  constructor( props ) {
    super( props );
    this.listaSeries = new ListaSeries()
    this.state = {
      validacao: false,
      filtroPorAno: false,
    }
    console.log( this.listaSeries.filtrarPorAno( 2020 ) );
    console.log( this.listaSeries.procurarPorNome( 'Winona Ryder' ) );
    console.log( this.listaSeries.mediaDeEpisodios() );
    console.log( this.listaSeries.totalSalarios( 2 ) );
    console.log( this.listaSeries.queroGenero( 'Caos' ) );
    console.log( this.listaSeries.queroTitulo( 'The' ) );
    console.log( this.listaSeries.creditos( 0 ) );
  }

  verificarInvalidas() {
    this.setState( {
      validacao: true
    } )
  }

  geraCampoInvalidas() {
    return (
      <div>
        { this.state.validacao && (
              <span> { this.listaSeries.invalidas() } </span>
          ) }
      </div>
    )
  }

  verificarAno( evt ) {
    this.setState( {
      filtroPorAno: true,
      ano: ( evt.target.value )
    })
  }

  geraCampoFiltrarPorAno() {
    return (
      <div>
        { this.state.filtroPorAno && (
            <div>
              <span> { this.listaSeries.filtrarPorAno( this.state.ano ) } </span>
            </div>
          ) }
      </div>
    )
  }

  render() {
    return (
      <div>
        <div className="App">
          <section className='App-section'>
            <h2>Info das Series JSFlix</h2>
            
            <button className="btn azul" onClick={ this.verificarInvalidas.bind( this ) }>Invalidas</button>
            <Card > { this.geraCampoInvalidas() } </Card> 
            <span>Estréia a partir de: <input  type="number" onBlur={ this.verificarAno.bind( this ) } ></input></span>
            <Card> { this.geraCampoFiltrarPorAno() } </Card>
            
                
          </section>
        </div>
        <Footer />
      </div>
      
    );
  }
}
export default JSFlix;
