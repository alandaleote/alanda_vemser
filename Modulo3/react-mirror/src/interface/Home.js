import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Footer from '../components/footer';

export default class Home extends Component {

  render() {
    return (
      <div>
        <div className="App-section">
          <h1>Escolha um caminho</h1>
          <div className="botoes">
            <Link to="/react-mirror"><button className="btn azul">React-Mirror</button></Link>
            <Link to="/js-flix"><button className="btn vermelho">JS-Flix</button></Link>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

