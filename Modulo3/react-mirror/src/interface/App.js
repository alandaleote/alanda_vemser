import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './Home';
import ReactMirror from './ReactMirror';
import JSFlix from './JSFlix';
import Avaliacoes from '../components/Avaliacoes';
import TelaDetalheEpisodio from '../components/TelaDetalheEpisodio';

export default class App extends Component {

  render() {
    return (
      <Router>
        <Route path="/" exact component={ Home } />
        <Route path="/react-mirror" component={ ReactMirror } />
        <Route path="/js-flix" component={ JSFlix } />
        <Route path="/avaliacoes" component={ Avaliacoes } />
        <Route path="/episodio/:id" component={ TelaDetalheEpisodio } />
      </Router>
    );
  }
}
