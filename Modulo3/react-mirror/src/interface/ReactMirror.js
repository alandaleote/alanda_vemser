import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/App.css';
import '../css/footer.css';
import ListaEpisodios from '../models/listaEpisodios';
import EpisodioUi from '../components/episodioUi';
import MensagemFlash from '../components/MensagemFlash';
import MeuInputNumero from '../components/MeuInputNumero';
import Footer from '../components/footer';


class ReactMirror extends Component {
  constructor( props ) {
    super( props );
    this.listaEpisodios = new ListaEpisodios();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false,
      exibirErro: false,
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState( {
      episodio
    } )
  }

  assistido() {
    const { episodio } = this.state
    episodio.marcarParaAssistido()
    this.setState( {
      episodio
    } )
  }

  registrarNota( { nota, erro } ){
    this.setState({
      exibirErro: erro 
    })
    if( erro ){
      return;
    }
    let cor, mensagem;
    const { episodio } = this.state
    if( episodio.validarNota( nota ) ) {
      episodio.avaliar( nota )
      cor= 'verde'
      mensagem= 'Registramos sua nota!'
    }else {
      cor= 'vermelho'
      mensagem= 'Informar uma nota válida (entre 1 e 5)'
    }
    this.exibirMensagem( { cor, mensagem } )
  }

  exibirMensagem = ( { cor, mensagem } ) => {
    this.setState( {
      cor,
      mensagem,
      exibirMensagem: true
    } )
  }

  atualizarMensagem = devoExibir => {
    this.setState({
        exibirMensagem: devoExibir
    })
  }

  render() {
    const { episodio, exibirMensagem, cor, mensagem, exibirErro, } = this.state
    const { listaEpisodios } = this

    return (
      <div>
        <div className="App">
          <MensagemFlash atualizarMensagem={ this.atualizarMensagem }
                          deveExibirMensagem={ exibirMensagem } 
                          mensagem={ mensagem }
                          cor={ cor }
                          segundos={ 5 }/>
          <section className='App-section'>
            <EpisodioUi episodio={ episodio } />
            <div className="botoes">
              <button className="btn verde" onClick={ this.sortear.bind( this ) }>Próximo</button>
              <button className="btn azul" onClick={ this.assistido.bind( this ) }>Já Assisti</button>
              <Link to={{ pathname: "/avaliacoes", state: {listaEpisodios} }}><button className="btn vermelho">Avaliações</button></Link>
            </div>
            <MeuInputNumero placeholder="1 a 5"
                            campoMensagem ="Qual sua nota para este episódio?"
                            exibir={ episodio.assistido || false }
                            atualizarValor={ this.registrarNota.bind( this ) }
                            exibirErro={ exibirErro } />
          </section>
        </div>
        <Footer />
      </div>
    );
  }
}

export default ReactMirror;

