export default class Serie {
    constructor( titulo, anoEstreia, diretor, genero, elenco, temporadas, numeroEpisodios, distribuidora ) {
        this.titulo = titulo
        this.anoEstreia = anoEstreia
        this.diretor = diretor
        this.genero = genero
        this.elenco = elenco
        this.temporadas = temporadas
        this.numeroEpisodios = numeroEpisodios
        this.distribuidora = distribuidora
    }
}
// var series = JSON.parse('[{"titulo":"Stranger Things","anoEstreia":2016,"diretor":["Matt Duffer","Ross Duffer"],"genero":["Suspense","Ficcao Cientifica","Drama"],"elenco":["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],"temporadas":2,"numeroEpisodios":17,"distribuidora":"Netflix"},{"titulo":"Game Of Thrones","anoEstreia":2011,"diretor":["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"],"genero":["Fantasia","Drama"],"elenco":["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],"temporadas":7,"numeroEpisodios":67,"distribuidora":"HBO"},{"titulo":"The Walking Dead","anoEstreia":2010,"diretor":["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"],"genero":["Terror","Suspense","Apocalipse Zumbi"],"elenco":["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"],"temporadas":9,"numeroEpisodios":122,"distribuidora":"AMC"},{"titulo":"Band of Brothers","anoEstreia":20001,"diretor":["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"],"genero":["Guerra"],"elenco":["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],"temporadas":1,"numeroEpisodios":10,"distribuidora":"HBO"},{"titulo":"The JS Mirror","anoEstreia":2017,"diretor":["Lisandro","Jaime","Edgar"],"genero":["Terror","Caos","JavaScript"],"elenco":["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"],"temporadas":1,"numeroEpisodios":40,"distribuidora":"DBC"},{"titulo":"10 Days Why","anoEstreia":2010,"diretor":["Brendan Eich"],"genero":["Caos","JavaScript"],"elenco":["Brendan Eich","Bernardo Bosak"],"temporadas":10,"numeroEpisodios":10,"distribuidora":"JS"},{"titulo":"Mr. Robot","anoEstreia":2018,"diretor":["Sam Esmail"],"genero":["Drama","Techno Thriller","Psychological Thriller"],"elenco":["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],"temporadas":3,"numeroEpisodios":32,"distribuidora":"USA Network"},{"titulo":"Narcos","anoEstreia":2015,"diretor":["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"],"genero":["Documentario","Crime","Drama"],"elenco":["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"],"temporadas":3,"numeroEpisodios":30,"distribuidora":null},{"titulo":"Westworld","anoEstreia":2016,"diretor":["Athena Wickham"],"genero":["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"],"elenco":["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],"temporadas":2,"numeroEpisodios":20,"distribuidora":"HBO"},{"titulo":"Breaking Bad","anoEstreia":2008,"diretor":["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"],"genero":["Acao","Suspense","Drama","Crime","Humor Negro"],"elenco":["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],"temporadas":5,"numeroEpisodios":62,"distribuidora":"AMC"}]')


// /* Nesse exercício deverá ser implementada uma função na prototype de Array chamada invalidas()
// que verifica quais são as séries inválidas e retorna seus títulos em formato String. 
// Exemplo:
// series.invalidas() // retorna "Séries Inválidas: Série 1 - Série 2 ..."
// Condições para as séries serem inválidas:
// Ano de estreia maior que o ano atual;
// Possuir algum campo que seja undefined ou null. */

// Array.prototype.invalidas = function () {
//     const invalidasArray = [];
//     function checkInvalida ( obj ) {
//         if ( obj.titulo === null || obj.anoEstreia === null || obj.anoEstreia > 2020 || obj.diretor === null || obj.genero === null || obj.elenco === null || obj.temporadas === null || obj.numeroEpisodios === null || obj.distribuidora === null ) ) {
//             obj.serieInvalida = true;
//             invalidasArray.push( obj.titulo );
//         }
//     }
//     this.forEach( checkInvalida );
//     return `Séries Inválidas: ${invalidasArray.join(' - ')}`
// }
    

// /* Nesse exercício deverá ser implementada uma função na prototype de Array chamada 
// filtrarPorAno(ano) que recebe o ano (number) e devolve um outro array contendo apenas as 
// séries com ano de lançamento maior ou igual ao ano passado por parâmetro.
// Exemplo:
// series.filtrarPorAno(2017) // 
// retorna um array com todas as séries com ano de estreia igual ou maior que 2017.*/


// Array.prototype.filtrarPorAno = function( ano ) {
//     function compararAnos ( obj ) {
//         return obj.anoEstreia >= ano;
//     }
//     return this.filter( compararAnos );
// }

// /* Crie uma função, na prototype de Array, chamada procurarPorNome(nome) que recebe um 
// nome e caso esse nome esteja no elenco das séries, retorna true. Caso contrário, false.
// Exemplo:
// serie.procurarPorNome("Bernardo") // retorna true se tiver um "Bernardo" em algum dos elencos
// Dica: No campo nome da função experimente passar seu próprio nome. */
// Array.prototype.procurarPorNome = function( nome ) {
//     function checkName ( obj ) {
//         elenco = [];
//         elenco = obj.elenco;
//         elenco = elenco.join(" ");
//         return elenco.includes( nome );
//     }
//     return this.some(  checkName );
// }

// /* Crie uma função, na prototype de Array, chamada mediaDeEpisodios() que retorna a média 
// dos episódios de todas as séries contidas no array.
// Exemplo:
// series.mediaDeEpisodios(); // 
// retorna o valor da média da soma dos episódios/quantidade de séries no array. */
// Array.prototype.mediaDeEpisodios = function() {
//     let totalEpisodios = 0
//     adicionarEpisodios = function( obj ) {
//         totalEpisodios += obj.numeroEpisodios;
//     }
//     this.forEach( adicionarEpisodios );

//     return (totalEpisodios / this.length)
// }


// /* Uma série tem seu elenco e diretor(es), mas para ela acontecer, eles devem ser pagos. 
// Crie uma função, na prototype de Array, chamada totalSalarios que recebe o índice da série 
// no array e retornará o valor total do salário a ser pago por mês. Para isso, suponha que os 
// Big-Bosses, os Diretores, ganhem R$ 100.000; Enquanto os operarios (cada um) ganha R$ 40.000;
// Exemplo:
// series.totalSalarios(0); //Retorna o valor total de gastos da série na posição 0, 
// contando os diretores e o elenco */
// Array.prototype.totalSalarios = function( index ) {
//     diretores = []
//     diretores = this[index].diretor
//     elenco = []
//     elenco = this[index].elenco
//     totalDiretores = ( diretores.length * 100000 )
//     totalOps = ( elenco.length * 40000 )
//     return ( totalDiretores + totalOps )
// }

// /* A) Não sei o que quero assitir, mas quero ver O CAOS! Escreva uma função, na prototype de Array, 
// chamada queroGenero que retorne um array, com os títulos das séries que são correspondentes com o 
// genero do parâmetro.
// Exemplo:
// series.queroGenero("Caos"); // Retorna ["The JS Mirror", "10 Days Why"] */

// Array.prototype.queroGenero = function (str) {
//     listaDeSeriesCompativeis = [];
//     this.forEach(obj => {
//         if (obj.genero.indexOf(str) >= 0) {
//             listaDeSeriesCompativeis.push(serie.titulo)
//         }
//     });
//     return listaDeSeriesCompativeis;
// }

// /* B) Sei exatamente o que quero assisitir! Escreva uma função, na prototype de Array, chamada 
// queroTitulo que retorne um array, com os títulos das séries que tem título semelhante ao passado
// Exemplo:
// serie.queroTitulo("The"); // Retorna ["The Walking Dead", "The JS Mirror"] */

// Array.prototype.queroTitulo = function (str) {
//     listaDeSeriesCompativeis = [];
//     this.forEach(obj => {
//         if (obj.titulo.indexOf(str) >= 0) {
//             listaDeSeriesCompativeis.push(obj.titulo)
//         }
//     });
//     return listaDeSeriesCompativeis;
// }


// /* Ao final de um episódio, temos os créditos do episódio. Para isso vamos implementar uma função, 
// na prototype de Array, chamada de creditos que imprima os créditos da série. Deverá ser impresso, o 
// Título da serie, os Diretores, avisando com um título que é o bloco deles. Em seguida vem o elenco, 
// também com um título de Elenco.
// Tranquilo né? Barbadinha! MAS, tem o seguinte: Os créditos são sempre ordenados alfabeticamente, 
// mas pelo ÚLTIMO NOME!! Faça os ajustes necessários para que isso seja possível.
// */




// /* Nos elencos das séries, quando abreviamos um nome colocamos a primeira letra do nome seguida de 
// um ponto final
// Exemplo:
// Bernardo Bosak Rezende -> Bernardo B. Rezende
// Tuanni Carvalho Miranda -> Tuanni C. Miranda
// Essa é a informação básica! Construa uma função, no prototype Array, que identificará aquela série 
// que tem TODOS do elenco com nomes abreviados.
// Dica: Construa uma função separada para identificar se aquela String tem a abreviação;
// Show de bola, estamos quase lá!
// Uma vez achada a série, vamos modificar um pouquinho a implementação. Coloque todas as palavras 
// abreviadas (de preferência sem os pontos finais) em uma string que será retornada ao final da função.
// Forme uma hashtag com a palavra! #PALAVRA resultante
// Me mande no slack, em privado, a hashtag que foi produzida.

//  */

//  Array.prototype.pesquisaElencoAbreviado = function () {
//     this.forEach(serie => {
//         iniciaisNaSerie = [];
//         elencoSoIniciais = true;
//         serie.elenco.forEach(ator => {
//             i = ator.indexOf(".");
//             if (i >= 0) {
//                 iniciaisNaSerie.push(ator.charAt(i-1))
//             } else if (i < 0) {
//                 elencoSoIniciais = false;
//             }
//         });
//         logElencoAbreviado(serie.titulo, elencoSoIniciais, iniciaisNaSerie);
//     });
// }

// function logElencoAbreviado ( serie, apenasIniciais, listaDeIniciais ) {
//     if ( apenasIniciais ) {
//         console.log("A série " + serie + " possuí apenas nomes abreviados, as iniciais combinadas compõe: " + listaDeIniciais );
//     }


