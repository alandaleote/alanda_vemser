import PropTypes from 'prop-types';
import Serie from './serie';

export default class ListaSeries {
    constructor() {
        this.todos = [
            {"titulo":"Stranger Things","anoEstreia":2016,"diretor":["Matt Duffer","Ross Duffer"],"genero":["Suspense","Ficcao Cientifica","Drama"],"elenco":["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],"temporadas":2,"numeroEpisodios":17,"distribuidora":"Netflix"},
            {"titulo":"Game Of Thrones","anoEstreia":2011,"diretor":["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"],"genero":["Fantasia","Drama"],"elenco":["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],"temporadas":7,"numeroEpisodios":67,"distribuidora":"HBO"},
            {"titulo":"The Walking Dead","anoEstreia":2010,"diretor":["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"],"genero":["Terror","Suspense","Apocalipse Zumbi"],"elenco":["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"],"temporadas":9,"numeroEpisodios":122,"distribuidora":"AMC"},
            {"titulo":"Band of Brothers","anoEstreia":20001,"diretor":["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"],"genero":["Guerra"],"elenco":["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],"temporadas":1,"numeroEpisodios":10,"distribuidora":"HBO"},
            {"titulo":"The JS Mirror","anoEstreia":2017,"diretor":["Lisandro","Jaime","Edgar"],"genero":["Terror","Caos","JavaScript"],"elenco":["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"],"temporadas":1,"numeroEpisodios":40,"distribuidora":"DBC"},
            {"titulo":"10 Days Why","anoEstreia":2010,"diretor":["Brendan Eich"],"genero":["Caos","JavaScript"],"elenco":["Brendan Eich","Bernardo Bosak"],"temporadas":10,"numeroEpisodios":10,"distribuidora":"JS"},
            {"titulo":"Mr. Robot","anoEstreia":2018,"diretor":["Sam Esmail"],"genero":["Drama","Techno Thriller","Psychological Thriller"],"elenco":["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],"temporadas":3,"numeroEpisodios":32,"distribuidora":"USA Network"},
            {"titulo":"Narcos","anoEstreia":2015,"diretor":["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"],"genero":["Documentario","Crime","Drama"],"elenco":["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"],"temporadas":3,"numeroEpisodios":30,"distribuidora":null},
            {"titulo":"Westworld","anoEstreia":2016,"diretor":["Athena Wickham"],"genero":["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"],"elenco":["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],"temporadas":2,"numeroEpisodios":20,"distribuidora":"HBO"},
            {"titulo":"Breaking Bad","anoEstreia":2008,"diretor":["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"],"genero":["Acao","Suspense","Drama","Crime","Humor Negro"],"elenco":["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],"temporadas":5,"numeroEpisodios":62,"distribuidora":"AMC"}
        ].map( serie => new Serie( serie.titulo, serie.anoEstreia, serie.diretor, serie.genero, serie.elenco, serie.temporadas, serie.numeroEpisodios, serie.distribuidora ) );
    }

    invalidas(){
        const invalidas = this.todos.filter( serie => {
            const algumCampoInvalido = Object.values( serie ).some( campo => campo === null || typeof campo === 'undefined' )
            const anoEstreiaInvalido = serie.anoEstreia > new Date().getFullYear()
            return algumCampoInvalido || anoEstreiaInvalido
        })
        return `Séries Inválidas: ${ invalidas.map( serie => serie.titulo ).join(' - ') }`
    }

    filtrarPorAno( ano ) {
        const filtradas = this.todos.filter( serie => {
            return serie.anoEstreia >= ano
        } )
        return `Séries encontradas: ${ filtradas.map( serie => serie.titulo ).join( ' - ' )  }`
    }

    procurarPorNome( nome ){
        let encontrou = false;
        this.todos.forEach( serie => {
            let achou = serie.elenco.find( n => nome === n )
            if( achou ){
                encontrou = true
            }
        })
        return encontrou
    }
    
    mediaDeEpisodios(){
        return parseFloat( this.todos.map( serie => serie.numeroEpisodios ).reduce( (prev, cur) => prev + cur, 0 ) / this.todos.length )
 
    }

    totalSalarios( index ) {
        function imprimirBRLOneLine( valor ) {
            return parseFloat( valor.toFixed( 2 ) ).toLocaleString('pt-BR', {
            //return parseFloat( valor.toFixed( 2 ) ).toLocaleString('pt-BR', {

                style: 'currency',
                currency: 'BRL'
            })
        }
        const salarioDirecao = 100000 * this.todos[ index ].diretor.length
        const salarioAtores = 40000 * this.todos[ index ].elenco.length
        return imprimirBRLOneLine( parseFloat( salarioDirecao + salarioAtores ) )
    }

    queroGenero( genero ) {
        return this.todos.filter( serie => {
            return Boolean( serie.genero.find( g => g === genero ) )
        } )
    }

    queroTitulo( titulo ) {
        let palavrasTitulo = titulo.split( ' ' )
        return this.todos.filter( serie => {
            let palavrasSeries =  serie.titulo.split( ' ' )
            return Boolean( palavrasSeries.find( palavra => palavra.includes( palavrasTitulo ) ) )
        } )
    }

    creditos( index ) {

        this.todos[ index ].elenco.sort( ( a, b ) => {
            let ArrayA = a.split( ' ' )
            let ArrayB = b.split( ' ' )
            return ArrayA[ ArrayA.length - 1 ].localeCompare( ArrayB[ ArrayB.length - 1 ] );
        })

        this.todos[ index ].diretor.sort( ( a, b ) => {
            let ArrayA = a.split( ' ' )
            let ArrayB = b.split( ' ' )
            return ArrayA[ ArrayA.length - 1 ].localeCompare( ArrayB[ ArrayB.length - 1 ] );
        })

        let arrayParaJuntar = []

        arrayParaJuntar.push( `${ this.todos[ index ].titulo }`)
        arrayParaJuntar.push( `Diretor(es)`)

        this.todos[ index ].diretor.forEach( d => {
            arrayParaJuntar.push( `${ d }`)
        })

        arrayParaJuntar.push( `Elenco`)

        this.todos[ index ].elenco.forEach( e => {
            arrayParaJuntar.push( `${ e }`)
        })

        return arrayParaJuntar;
    }

    matchesNomesAbreviados( s ) {
        let verifica = s.match( '\\s+[a-zA-Z]{1}\\.\\s+')
        return ( verifica ) ? Boolean( verifica[0] ) : undefined 
    }

    serieComNomesAbreviados(){
        this.todos.filter( serie => {
            return !this.serieComNomesAbreviados.elenco.find( s => !this.matchesNomesAbreviados( s ) )
        })
    }

    letrasAbreviadas( s ){
        if( this.matchesNomesAbreviados( s ) ) return s.match( '\\s+[a-zA-Z]{1}\\.\\s+')[0].charAt(1) 
    }

    mostrarPalavraSecreta() {
        let palavra = '#';
        let array = this.serieComNomesAbreviados()
        array[0].elenco.forEach( s => {
            palavra = palavra.concat( this.letrasAbreviadas( s ) )
        })
        return palavra
    }

/* Nos elencos das séries, quando abreviamos um nome colocamos a primeira letra do nome seguida de 
um ponto final
Exemplo:
Bernardo Bosak Rezende -> Bernardo B. Rezende
Tuanni Carvalho Miranda -> Tuanni C. Miranda
Essa é a informação básica! Construa uma função, no prototype Array, que identificará aquela série 
que tem TODOS do elenco com nomes abreviados.
Dica: Construa uma função separada para identificar se aquela String tem a abreviação;
Show de bola, estamos quase lá!
Uma vez achada a série, vamos modificar um pouquinho a implementação. Coloque todas as palavras 
abreviadas (de preferência sem os pontos finais) em uma string que será retornada ao final da função.
Forme uma hashtag com a palavra! #PALAVRA resultante
Me mande no slack, em privado, a hashtag que foi produzida.

 */

    // Array.prototype.pesquisaElencoAbreviado = function () {
    //     this.forEach(serie => {
    //         iniciaisNaSerie = [];
    //         elencoSoIniciais = true;
    //         serie.elenco.forEach(ator => {
    //             i = ator.indexOf(".");
    //             if (i >= 0) {
    //                 iniciaisNaSerie.push(ator.charAt(i-1))
    //             } else if (i < 0) {
    //                 elencoSoIniciais = false;
    //             }
    //         });
    //         logElencoAbreviado(serie.titulo, elencoSoIniciais, iniciaisNaSerie);
    //     });
    // }

    // function logElencoAbreviado ( serie, apenasIniciais, listaDeIniciais ) {
    //     if ( apenasIniciais ) {
    //         console.log("A série " + serie + " possuí apenas nomes abreviados, as iniciais combinadas compõe: " + listaDeIniciais );
    //     }
    // }

}


ListaSeries.PropTypes = {
    invalidas: PropTypes.array,
    filtrarPorAno: PropTypes.array,
    procurarPorNome: PropTypes.array,
    mediaDeEpisodios: PropTypes.array,
    totalSalarios: PropTypes.array,
    queroGenero: PropTypes.array,
    queroTitulo: PropTypes.array,
    creditos: PropTypes.array,
}


