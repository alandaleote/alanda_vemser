export default class Episodio {
    constructor( id, nome, duracao, temporada, ordemEpisodio, thumbUrl ) {
        this.id = id
        this.nome = nome
        this.duracao = duracao
        this.temporada = temporada
        this.ordem = ordemEpisodio
        this.url = thumbUrl
        this.qtdVezesAssistido = 0
        this.avaliacao = []
    }

    marcarParaAssistido() {
        this.assistido = true
        this.qtdVezesAssistido += 1
    }

    validarNota( nota ) {
        nota = parseInt( nota )
        return 1 <= nota && nota <= 5
    }

    avaliar( nota ) {
        nota = parseInt( nota )
        this.avaliacao.push( nota )
        this.nota = parseInt( this.calcularNotaMedia() )
    }

    calcularNotaMedia(){
        const media = parseInt( this.avaliacao.reduce( (prev, cur) => prev + cur, 0 ) / this.avaliacao.length )
        return media
    }


    get duracaoEmMin(){
        return `${ this.duracao } min`
    }

    get temporadaEpisodio() {
        return `${ this.temporada.toString().padStart( 2, '0' ) }/${ this.ordem.toString().padStart( 2, '0' ) }`
    }

    get notaMedia() {
        return `${ this.avaliacao.length > 0 ? this.calcularNotaMedia() : '' }`
    }
}