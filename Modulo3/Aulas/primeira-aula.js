//console.log("Cheguei", "Aqui");

var nomeDaVariavel = "valor";
let nomeDaLet = "valorLet"; //local - declara uma vez, mas pode alterar o valor.
const nomeDaConst = "valorConst"; // constante, não altera o valor.

var nomeDaVariavel = "valor";
nomeDaLet = "valorLet2";

const nomeDaConst2 = {
    nome:"Marcos",
    idade: 29
};

Object.freeze(nomeDaConst2);

nomeDaConst2.nome = "Marcos Henrique"

//console.log(nomeDaConst2);

function nomeDaFuncao(){
    //nomeDaVariavel = "valor";

};
/*
function somar(){

}*/

function somar(valor1, valor2 = 1){
    console.log(valor1 + valor2);

}
/*
somar(3);
somar(3, 2);

console.log(1+1);
console.log(1+"1");
console.log(1 + "1" + 1);
*/ 

function ondeMoro(cidade){
    //console.log("Eu moro em " + cidade + ". E sou feliz");
    console.log(`Eu moro em ${cidade}. E sou feliz ${ 30 +1} dias.`);
}

//ondeMoro("Porto Alegre");

function fruteira(){
    let texto = "Banana"
                + "\n"
                + "Ameixa"
                + "\n"
                + "Goiaba"
                + "\n"
                + "Pessego"
                + "\n";

    let newTexto = `
                Banana
                    Ameixa
                        Goiaba
                            Pessego
                `;

    console.log(texto);
    console.log(newTexto);
}

//fruteira();

let eu = {
    nome: "Alanda",
    idade: 29,
    altura: 1.55
};

function quemSou(pessoa){
    console.log(`Meu nome é ${pessoa.nome}, tenho ${pessoa.idade} anos e ${pessoa.altura} de altura.`);
};

//quemSou(eu);

let funcaoSomarValores = function(a, b){
    return a + b;
};

let add = funcaoSomarValores;
let resultado = add(3, 5);

//console.log(resultado);


const { nome:n, idade:i } = eu;
//console.log(nome, idade);
//console.log(n, i);

const array = [1, 3, 4, 8];
const [n1, ,n3, n2,n4 = 18] = array;
//console.log(n1, n2, n3, n4);


function testarPessoa({ nome, altura, idade}){
    console.log( nome, idade, altura);
}

//testarPessoa(eu);

let a1 = 42;
let b1 = 15;
let c1 = 99;
let d1 = 109;

/*let aux = a1;
a1 = b1;
b1 = aux; 
console.log(a1, b1, c1, d1);

[a1, b1, c1, d1] = [b1, d1, a1, c1];

console.log(a1, b1, c1, d1);*/