
//Exercício 1
let obj1 = {
    raio: 3,
    tipoCalculo: "A"
}

let obj2 = {
    raio: 3,
    tipoCalculo: "C"
}

function calcularCirculo({raio, tipoCalculo}){
    if (tipoCalculo == "A") {
        return Math.ceil(Math.PI * Math.pow( raio , 2));
    }
    if (tipoCalculo == "C") {
        return Math.ceil(2 * Math.PI * raio);
    }
}

//console.log(calcularCirculo(obj1));
//console.log(calcularCirculo(obj2));


//Exercício 2

function naoBissexto(ano){
    return (ano % 4 === 0 && ano % 100 !== 0 || ano % 400 === 0) ? false : true;
}

let bissexto = ano => (ano % 4 === 0 && ano % 100 !== 0 || ano % 400 === 0);

//console.log(naoBissexto(2016));
//console.log(naoBissexto(2017));


//Exercício 3

let lista = [ 1, 56, 4.34, 6, -2 ];

function somarPares(array){
    let soma = 0
    for (let i = 0; i < array.length; i+=2) {
        soma += array[i]; 
    }
    return soma;
}

//console.log(somarPares(lista));

//Exercício 4

/*function adicionar(op1){
    return function(op2){
        return op1 + op2;
    }
} */

let adicionar = op1 => op2 => op1 +op2;


const divisivelPor = divisor => numero => !(numero%divisor);
const is_divisivel = divisivelPor(2);

//console.log(is_divisivel(20));

