import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Login from './pages/login';
import { isAuthenticated } from './autenticacao'
import Home from './pages/home'
import Agencias from './pages/agencias'
import AgenciaId from './components/agenciaId'
import Clientes from './pages/clientes'
import ClienteId from './components/clienteId'
import TipoContas from "./pages/tipoConta";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated() ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: "/", state: { from: props.location } }} />
      )
    }
  />
);

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={ Login } />
      <Route exact path="/login" component={ Login } />
      <PrivateRoute exact path="/home" component={ Home } />
      <PrivateRoute exact path="/agencias" component={ Agencias } />
      <PrivateRoute exact path="/agencias/:id" component={ AgenciaId } />
      <PrivateRoute exact path="/clientes" component={ Clientes } />
      <PrivateRoute exact path="/clientes/:id" component={ ClienteId } />
      <PrivateRoute exact path="/tipoContas" component={ TipoContas } />

      <Route path="*" component={() => <h1>Error 404 - Page not found</h1>} />
    </Switch>
  </BrowserRouter>
);

export default Routes;