import React, { Component } from 'react';
import axios from 'axios';
import '../App.css'
import { authToken } from '../autenticacao';
import { Link } from 'react-router-dom';
import Header from '../components/header'


class Clientes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clientes: [],
            isLoaded: false,
        }
    }

    componentDidMount() {
        this.callList();
    }

    async callList() {
        try {
            axios.get('http://localhost:1337/clientes', { headers: { authorization: authToken } })
                .then(res => {
                    this.setState({
                        isLoaded: true,
                        clientes: res.data.clientes
                    })
                })
        } catch (error) {
            console.error(error);
        }
    }

    render() {
        const { clientes, isLoaded } = this.state;
        if (!isLoaded) {
            return <div>Loading...</div>
        }
        else {

            return (
                <div>
                    <Header />
                    <div>
                        <section className='App-section'>
                            <h1>Clientes</h1>
                            {
                                clientes.map(cl => {
                                    return (
                                        < div className='line' key={cl.id}>
                                            <Link to={{ pathname: `/clientes/${cl.id}`, state: { cliente: cl } }}>
                                                {`Nome do cliente: ${cl.nome}`}
                                            </Link>
                                        </div>
                                    )
                                })
                            }
                        </section>

                    </div>
                </div>


            )
        }
    }
}
export default Clientes;
