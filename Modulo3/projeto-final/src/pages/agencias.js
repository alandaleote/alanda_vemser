import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import '../App.css'
import { authToken } from '../autenticacao';
import Header from '../components/header'


export default class Agencias extends Component {
    constructor( props ) {
        super( props );
        this.state = {
            agencias: [],
            isLoaded: false,
            
        }
    }
 
    componentDidMount() {
        this.callList();
    }

    async callList() {
        try {
            axios.get('http://localhost:1337/agencias', { headers: { authorization: authToken } })
                .then(res => {
                    this.setState({
                        isLoaded: true,
                        agencias: res.data.agencias
                    })
                })
        } catch (error) {
            console.error(error);
        }
    }
    
    render() {
        const { agencias, isLoaded } = this.state;
        if( !isLoaded ) {
            return <div>Loading...</div>
        }
        else {

            return (
                <div>
                    <Header/>
                    <section className='App-section'>
                        <h1>Agências</h1>
                        {
                        agencias.map( ag => {
                            return (
                            <div className='line' key={ ag.id }>
                                <Link to={ { pathname: `/agencias/${ ag.id }`, state: { agencia: ag } } }>
                                    { `Código: ${ ag.codigo.toString().padStart( 4, '0' ) } - Agência: ${ ag.nome }` }
                                </Link>
                            </div>
                        ) }
                        ) }
                    </section>
                
                </div>
                
            )
        }
    }
}
