import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from '../components/header'
import logo from '../img/logo-vertical.png'

export default class Home extends Component {

  render() {
    return (
      <div>
        <Header/>
        <div className="App-section">
          <div>
            <img src={ logo } alt="Banco Digital"/>
          </div>
          <div className="inline">
            <span>
              <Link to="/agencias"><h3>Agências</h3></Link>
            </span>
            <span>
              <Link to="/clientes"><h3>Clientes</h3></Link>
            </span>
            <span>
              <Link to="/tipoContas"><h3>Contas</h3></Link>
            </span>
            
            
            
          </div>
        </div>
      </div>
    );
  }
}