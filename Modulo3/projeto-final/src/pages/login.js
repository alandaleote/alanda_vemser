import React, { Component } from 'react';
import axios from 'axios';
import { login } from  '../autenticacao';
import { withRouter } from "react-router-dom";
import '../App.css';
import Header from '../components/header'

class Login extends Component {
  state = {
    email: "",
    senha: "",
    error: ""
  };

  validarLogin = async e => {
    e.preventDefault();
    const { email, senha } = this.state;
    if (!email || !senha) {
      this.setState({ error: "Preencha e-mail e senha para continuar!" });
    } else {

      try {
        const response = await axios.post("http://localhost:1337/login", { email, senha });
        login(response.data.token);
        this.props.history.push("/home");
      } catch (err) {
        this.setState({
          error:
            "Login inválido."
        });
      }
    }
  };

  render() {
    return (
      <div>
        <Header/>
        <div className='App-section'>
          <form onSubmit={this.validarLogin}>
            {this.state.error && <span>{this.state.error}</span>}
            <input
              type="email"
              placeholder="E-mail"
              onChange={e => this.setState({ email: e.target.value })}
            />
            <input
              type="password"
              placeholder="Senha"
              onChange={e => this.setState({ senha: e.target.value })}
            />
            <button className="btn azul" type="submit">Entrar</button>
          </form>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);

