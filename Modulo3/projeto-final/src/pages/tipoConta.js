import React, { Component } from 'react';
import axios from 'axios';
import '../App.css'
import { authToken } from '../autenticacao';
import Table from '../components/table';


export default class TipoContas extends Component {
    constructor( props ) {
        super( props );
        this.state = {
            tipoContas: [],
        }
    }

    getTipoContas = async () => {
        const response =
            await axios.get('http://localhost:1337/tipoContas', { headers: { authorization : authToken } } );
        this.setState({ tipoContas: response.data.tipos });
        return response.data.tipos
    }
    
    componentDidMount() {
        this._asyncRequest = this.getTipoContas();
        }  

    render() {
        const { tipoContas } = this.state;

        return (
            <React.Fragment>
                <div className='App-section'>
                    <span><h1>Tipos de Conta</h1></span>
                    <Table>
                        {
                            <tr>
                                <th>id</th>
                                <th>nome</th>
                            </tr>
                        }
                        { 
                            tipoContas.map( tipoContas => <tr>
                                <td>{ tipoContas.id }</td>
                                <td>{ tipoContas.nome }</td>
                            </tr> ) 
                        }
                    </Table>
                </div>
            </React.Fragment>
        );
    }
}