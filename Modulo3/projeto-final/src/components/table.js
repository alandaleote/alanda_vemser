import React from 'react';

const Table = props => (
    <table className='table'>
        <thead>
            {props.children}
        </thead>
        <tbody>
            
        </tbody>
    </table>
)
    
export default Table