import React, { Component } from 'react';
import '../App.css';
import Header from '../components/header'

export default  class AgenciaId extends Component {

    constructor( props ) {
        super( props );
        this.state = {
            agenciaId: [],
            isLoaded: false,
            
        }
    }

    componentDidMount() {
        const { agencia } =  this.props.location.state
        this.setState({
            isLoaded: true,
            agenciaId:  agencia });
    }
   
    render() {

        const { isLoaded, agenciaId } = this.state;

        if( !isLoaded ) {
            return <div>Loading...</div>
        }
        else {
            return (
                <div>
                    <Header/>
                    <div className="App-section">
                        <h1>Agência</h1>
                        <div>
                            <span className='line' >
                                { `Código: ${agenciaId.codigo.toString().padStart( 4, '0' ) }` }
                            </span>
                            <span className='line' >
                                { `Agência: ${ agenciaId.nome }` }
                            </span>
                            <span className='line' >
                                { `Endereço: ${ agenciaId.endereco.logradouro }, ${ agenciaId.endereco.numero }` }
                            </span>
                            <span className='line' >
                                { `Bairro: ${agenciaId.endereco.bairro}, ${agenciaId.endereco.cidade}/${agenciaId.endereco.uf}` }
                            </span>
                        </div>
                    </div>
                </div>
            )
        }
    }
}



