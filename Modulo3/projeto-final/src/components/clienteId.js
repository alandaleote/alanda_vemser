import React, { Component } from 'react';
import '../App.css';
import Header from '../components/header'

export default class ClienteId extends Component {

    constructor(props) {
        super(props);
        this.state = {
            clienteId: [],
            isLoaded: false,
        }
    }

    componentDidMount() {
        const { cliente } = this.props.location.state
        this.setState({ clienteId: cliente, isLoaded: true });
    }

    render() {

        const { isLoaded, clienteId } = this.state;

        if (!isLoaded) {
            return <div>Loading...</div>
        }
        else {
            return (
                <div>
                    <Header/>
                    <div className="App-section">
                        <h1>Cliente</h1>
                        <div>
                            <span className='line' >
                                {`Nome: ${clienteId.nome}`}
                            </span>
                            <span className='line' >
                                {`CPF: ${clienteId.cpf}`}
                            </span>
                            <span className='line' >
                                {`Agência: ${clienteId.agencia.codigo.toString().padStart(4, '0')}`}
                            </span>
                        </div>
                    </div>
                </div>
            )
        }
    }
}