import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './header.css'
import logo from '../img/logo-horizontal.png';

export default class Header extends Component {
    render() {
      return (
        <React.Fragment>
        <header className='main-header'>
            <div>
                <nav>
                    <div class="logo" >
                    <img src={ logo } alt="Banco Digital"/>
                    </div>
                    <ul className='clearfix'>
                        <li>
                            <Link to="/home">Home</Link>
                        </li>
                        <li>
                            <Link to="/agencias">Agências</Link>
                        </li>
                        <li>
                            <Link to="/clientes">Clientes</Link>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
        </React.Fragment>
      )
    }
  }

