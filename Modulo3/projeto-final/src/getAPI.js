import axios from 'axios';

import { authToken } from './autenticacao';

export const getAgenciasAxios = async () => {
        const response =
            await axios.get('http://localhost:1337/agencias', { headers: { authorization : authToken } } );
        this.setState({ agencias: response.data.agencias });
        return response.data.agencias
    }

export const getClientesAxios = async () => {
        const response =
            await axios.get('http://localhost:1337/clientes', { headers: { authorization : authToken } } );
        // this.setState({ clientes: response.data.clientes });
        return response.data.clientes
    }

export const getTipoContasAxios = async () => {
        const response =
            await axios.get('http://localhost:1337/tiposContas', { headers: { authorization : authToken } } );
        this.setState({ tipoContas: response.data.tipoContas });
        return response.data.tipoContas
    }

    export class GetApi {

        getContas = () => axios.get('http://localhost:1337/tiposContas', { headers: { authorization : authToken } } );


        getContaEspecifica = (indice) => axios.get(`http://localhost:1337/tiposConta/${indice}`,{ headers: { authorization : authToken } } );
    
    
    }
