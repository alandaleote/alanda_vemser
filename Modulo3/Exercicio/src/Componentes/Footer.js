import React, { Component } from 'react';

export default class Footer extends Component {
    render() {
      
      return (
        <React.Fragment>
        <footer className='main-footer'>
            <div className='container'>
                <nav>
                    <ul className='clearfix'>
                        <li>
                            <a href='#'>Home</a>
                        </li>
                        <li>
                            <a href='#'>Sobre nós</a>
                        </li>
                        <li>
                            <a href='#'>Serviços</a>
                        </li>
                        <li>
                            <a href='#'>Contato</a>
                        </li> 
                    </ul>
                </nav>
                <p>
                    &copy; Copyright DBC Company - 2019
                </p>
            </div>
        </footer>
        </React.Fragment>
      )
    }
  }

