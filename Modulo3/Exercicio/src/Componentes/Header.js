import React, { Component } from 'react';

import Logo from '../img/dbc_logo.png';

export default class Header extends Component {
    render() {
      
      return (
        <React.Fragment>
        <header className='main-header'>
            <nav className='container clearfix'>
                <a className='logo' href='index.html' title='Voltar à home'>
                    <img src={ Logo } alt='DBC Company'></img>
                </a>

                <label className='mobile-menu' for='mobile-menu'>
                    <span></span>
                    <span></span>
                    <span></span>
                </label>
                <input id='mobile-menu' type='checkbox'></input>

                <ul className='clearfix'>
                    <li>
                        <a href='index.html'>Home</a>
                    </li>
                    <li>
                        <a href='sobre.html'>Sobre nós</a>
                    </li>
                    <li>
                        <a href='servicos.html'>Serviços</a>
                    </li>
                    <li>
                        <a href='contato.html'>Contato</a>
                    </li> 
                </ul>
            </nav>
        </header>
        </React.Fragment>
      )
    }
  }

