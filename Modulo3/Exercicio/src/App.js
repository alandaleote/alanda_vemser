import React, { Component } from 'react';
import Header from './Componentes/Header'
import './css/banner.css';
import './css/box.css';
import './css/button.css';
import './css/contato.css';
import './css/footer.css';
import './css/general.css';
import './css/grid.css';
import './css/header.css';
import './css/reset.css';
import './css/sobre.css';
import './css/work.css';


import Agil from './img/Agil.png';
import Smart from './img/Smartsourcing.png';
import Builder from './img/Software-Builder.png';
import Sustain from './img/Sustain.png';


class App extends Component {
  // constructor( props ) {
  //   super( props )
  //   this.page = new Page();
    
  // }

  render() {
    return (
      <div className='App'>
        <Header />
        <section className='main-banner'>
            <article>
                <h1>Vem Ser DBC</h1>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum deserunt qui velit perferendis officia necessitatibus enim optio nobis? Consectetur qui odio alias mollitia neque officia laudantium tenetur nostrum aperiam fugit.
                    <span>
                        <a className='button button-blue' href='#'>Saiba mais</a>
                    </span>
                </p>
                
            </article>
        </section>

        <section className='container'>
            <div className='row'>
                <article className='col col-8'>
                    <h2>Título</h2>
                    <p className='justify'>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt dolore eius quis, voluptatibus corrupti officiis voluptatem, unde quisquam ratione ea maiores quod voluptates suscipit perferendis optio at atque cupiditate. Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore repellat cupiditate similique odio voluptatibus. Ad minus animi obcaecati quaerat, nam fugit odio molestias, ullam dolor quam cumque eos! Adipisci, optio?
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt dolore eius quis, voluptatibus corrupti officiis voluptatem, unde quisquam ratione ea maiores quod voluptates suscipit perferendis optio at atque cupiditate. Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore repellat cupiditate similique odio voluptatibus. Ad minus animi obcaecati quaerat, nam fugit odio molestias, ullam dolor quam cumque eos! Adipisci, optio?
                    </p>
    
                </article>
                <article className='col col-4'>
                    <h3>Título</h3>
                    <p className='justify'>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt dolore eius quis, voluptatibus corrupti officiis voluptatem, unde quisquam ratione ea maiores quod voluptates suscipit perferendis optio at atque cupiditate. Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore repellat cupiditate similique odio voluptatibus. Ad minus animi obcaecati quaerat, nam fugit odio molestias, ullam dolor quam cumque eos!
                    </p>
    
                </article>
            </div>
            <div className='row'>
                <div className='col col-6'>
                    <article className='box'>
                        <div>
                            <img src={ Agil }></img>
                        </div>
                        <h4>Agil</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore et animi quidem quas doloremque tempore aut consequatur perferendis ducimus neque unde, at suscipit voluptatum cumque odio explicabo natus dolorum. Tempora.
                        </p>
                        <a className='button button-blue'>Saiba mais</a>
                    </article>
                </div>
                <div className='col col-6'>
                    <article className='box'>
                        <div>
                            <img src={ Smart }></img>
                        </div>
                        <h4>Smartsourcing</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore et animi quidem quas doloremque tempore aut consequatur perferendis ducimus neque unde, at suscipit voluptatum cumque odio explicabo natus dolorum. Tempora.
                        </p>
                        <a className='button button-blue'>Saiba mais</a>
                    </article>    
                </div>
                <div className='col col-6'>
                    <article className='box'>
                        <div>
                            <img src={ Builder }></img>
                        </div>
                        <h4>Software-Builder</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore et animi quidem quas doloremque tempore aut consequatur perferendis ducimus neque unde, at suscipit voluptatum cumque odio explicabo natus dolorum. Tempora.
                        </p>
                        <a className='button button-blue'>Saiba mais</a>
                    </article>    
                </div>
                <div className='col col-6'>
                    <article className='box'>
                        <div>
                            <img src={ Sustain }></img>
                        </div>
                        <h4>Sustain</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore et animi quidem quas doloremque tempore aut consequatur perferendis ducimus neque unde, at suscipit voluptatum cumque odio explicabo natus dolorum. Tempora.
                        </p>
                        <a className='button button-blue'>Saiba mais</a>
                    </article>    
                </div>
            </div>
        </section>

        <footer className='main-footer'>
            <div className='container'>
                <nav>
                    <ul className='clearfix'>
                        <li>
                            <a href='#'>Home</a>
                        </li>
                        <li>
                            <a href='#'>Sobre nós</a>
                        </li>
                        <li>
                            <a href='#'>Serviços</a>
                        </li>
                        <li>
                            <a href='#'>Contato</a>
                        </li> 
                    </ul>
                </nav>
                <p>
                    &copy; Copyright DBC Company - 2019
                </p>
            </div>
        </footer>
      </div>
    );
  }
}

export default App;
