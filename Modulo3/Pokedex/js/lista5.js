let insert = document.getElementById('insert');
let id = document.getElementById('id');
let name = document.getElementById('name');
let height = document.getElementById('height');
let weight = document.getElementById('weight');
let types = document.getElementById('types');
let stats = document.getElementById('stats');
let pokemonImage = document.getElementById('poke-img');
let pokemonLogo = document.getElementById('poke-logo');
let button = document.getElementById('com-sorte');
let generalInfo = document.getElementById('generalInfo');

const validation = value => {
    if (value < 0 || value > 803) {
        return false;
    } else {
        return true;
    }
}; 

const samePokemon = value => {

    if (value == id.innerHTML) {
        return true;
    } else {
        return false;
    }
}

const createErrorMessage = () => {
    const divErro = document.getElementById('div-erro');
    const spanArroba = document.createElement("span");

    spanArroba.innerHTML = "Este campo deve conter um id válido.";
    spanArroba.style.color = "red";
    spanArroba.style.display = "block";
    spanArroba.style.textAlign = "center";
    divErro.insertAdjacentElement('afterbegin', spanArroba)

    setInterval(() => {
        spanArroba.style.display = "none";
    }, 5000
    )
};

const refreshValues = (infoBlock, typesBlock) => {

    id.innerHTML = "";
    name.innerHTML = "";
    height.innerHTML = "";
    weight.innerHTML = "";
    pokemonImage.src = "./img/whoisthispokemon.png";
    types.innerHTML = "";
    stats.innerHTML = "";
}

const populateElements = valor => {

    let response = fetch(`https://pokeapi.co/api/v2/pokemon/${valor}`);

    setTimeout(() => {
        response
            .then(data => data.json())
            .then(data => {
                id.innerHTML = data.id;
                name.innerHTML = data.name.replace(/^./, data.name[0].toUpperCase());
                height.innerHTML = `${data.height * 10}cm`;
                weight.innerHTML = `${data.weight / 10}kg`;

                pokemonImage.src = data.sprites.front_default;
                pokemonImage.style.margin = "0 auto";
                pokemonImage.style.height = "190px";

                const statsArray = [...data.stats];
                const typesArray = [...data.types];

                statsArray.map(elemento => {
                    let div = document.createElement('div');
                    div.className = "mt-2";
                    div.innerHTML = elemento.base_stat;
                    stats.appendChild(div);
                });

                typesArray.map(elemento => {
                    let li = document.createElement('li');
                    li.innerHTML = elemento.type.name;

                    switch (elemento.type.name) {
                        case 'normal':
                            li.className = "line-item normal";
                            break;
                        case 'fire':
                            li.className = "line-item fire";
                            break;
                        case 'water':
                            li.className = "line-item water";
                            break;
                        case 'electric':
                            li.className = "line-item electric";
                            break;
                        case 'grass':
                            li.className = "line-item grass";
                            break;
                        case 'ice':
                            li.className = "line-item ice";
                            break;
                        case 'fighting':
                            li.className = "line-item fighting";
                            break;
                        case 'poison':
                            li.className = "line-item poison";
                            break;
                        case 'ground':
                            li.className = "line-item ground";
                            break;
                        case 'flying':
                            li.className = "line-item flying";
                            break;
                        case 'psychic':
                            li.className = "line-item psychic";
                            break;
                        case 'bug':
                            li.className = "line-item bug";
                            break;
                        case 'rock':
                            li.className = "line-item rock";
                            break;
                        case 'ghost':
                            li.className = "line-item ghost";
                            break;
                        case 'dragon':
                            li.className = "line-item dragon";
                            break;
                        case 'dark':
                            li.className = "line-item dark";
                            break;
                        case 'steel':
                            li.className = "line-item steel";
                            break;
                        case 'fairy':
                            li.className = "line-item fairy";
                            break;
                        default:
                            li.className = "line-item";
                    }

                    types.appendChild(li);
                });
            });
    }, 1000);
}

insert.addEventListener('blur', () => {
    let valorDigitado = insert.value;

    if ( samePokemon(valorDigitado) ) {
        return false;
    } else {
        refreshValues(generalInfo, types);

        if(validation(valorDigitado)) {
            populateElements(valorDigitado);
        } else {
            createErrorMessage();
        }
    }
});

button.addEventListener('click', () => {
    insert.value = "";
    let minId = 1; 
    let maxId = 5;  
    let randomId = Math.floor(Math.random() * (maxId - minId) + minId); 
    console.log(randomId);

    refreshValues(generalInfo, types);
    populateElements(randomId);
});