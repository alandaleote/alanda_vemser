import java.util.*;

public class Inventario {
    
    private ArrayList<Item> itens;
    
    public Inventario() {
        this.itens = new ArrayList<>();
    }
    
    public void adicionar( Item novoItem ) {
        this.itens.add( novoItem );
    }
    
    public ArrayList<Item> getInventario() {
        return this.itens;
    }
    
    public Item obter( int posicao ) {
        if( posicao >= this.itens.size() ) {
            return null;
        }
        return this.itens.get(posicao);
        }
     
    public void remover( Item novoItem ) {
         this.itens.remove( novoItem );
    }
    
    public Item buscar( String descricao ) {
        for( int i = 0; i < this.itens.size(); i++ ) {
            Item buscarItem = this.itens.get(i);
            if ( descricao == buscarItem.getDescricao() ) {
                return buscarItem;
            }
        }
        return null;
    }
    
    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        for(int i = 0; i < this.itens.size(); i++){
            Item item = this.itens.get(i);
            descricoes.append(item.getDescricao());
            descricoes.append(", ");      
        }

        return (descricoes.length() > 0 ? descricoes.substring(0, (descricoes.length() - 2)) : descricoes.toString());
    }
    
    public Item getItemComMaiorQuantidade() {
        int indice = 0, maiorQuantidade = 0;
        
        for( int i = 0; i < this.itens.size(); i++ ) {
            Item item = this.itens.get(i);
            if ( item.getQuantidade() > maiorQuantidade ) {
                maiorQuantidade = item.getQuantidade();
                indice = i;
            }
        }
        return this.itens.isEmpty() ? null : this.itens.get(indice);
    }
    
    public ArrayList<Item> inverter() {
        ArrayList<Item> inventarioInvertido = new ArrayList<>();
        for(int i = this.itens.size() - 1; i >= 0 ; i--) {
            inventarioInvertido.add(this.itens.get(i));
        }
        return inventarioInvertido;
    }
    
    public void ordenarItens() {
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
    public void ordenarItens( TipoOrdenacao ordenacao) {
        /*if ( tipo == TipoOrdenacao.ASC) {
            Collections.sort(itens, Comparator.comparingInt(Item::getQuantidade));
        }
        else if ( tipo == TipoOrdenacao.DESC) {
            Collections.sort(itens, Comparator.comparingInt(Item::getQuantidade).reversed());
        }*/
        
        for ( int i = 0; i < this.itens.size(); i++){
            for ( int j = 0; j < this.itens.size() -1; j++){
                Item atual = this.itens.get(j);
                Item proximo = this.itens.get(j + 1);
                
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ? 
                atual.getQuantidade() > proximo.getQuantidade() : 
                atual.getQuantidade() < proximo.getQuantidade();
                
                if ( deveTrocar ) {
                    Item itemTrocado = atual;
                    this.itens.set(j, proximo );
                    this.itens.set(j + 1, itemTrocado);
                }
            }
        }
    }
    

}
    