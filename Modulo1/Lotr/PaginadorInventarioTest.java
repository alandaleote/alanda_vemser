

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class PaginadorInventarioTest
{
    @Test
    public void mudarMarcadorComPular() {
        Inventario novoInventario = new Inventario();
        PaginadorInventario paginador = new PaginadorInventario( novoInventario );
        paginador.pular(2);
        assertEquals( 2, paginador.getMarcador() );
    }
    
    @Test
    public void limitarInventario() {
        Inventario novoInventario = new Inventario();
        
        Item item0 = new Item( 1, "Arco" );
        Item item1 = new Item( 2, "Flecha" );
        Item item2 = new Item( 1, "Escudo" );
        Item item3 = new Item( 7, "Moeda" );
        Item item4 = new Item( 2, "Adaga" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        novoInventario.adicionar(item2);
        novoInventario.adicionar(item3);
        novoInventario.adicionar(item4);
        
        PaginadorInventario paginador = new PaginadorInventario( novoInventario );
        paginador.limitar(3);
        
        
    }
    
}
