

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;


public class DwarfBardaLongaTest{
    @Test
    public void tira3eSofreDano(){
        DadoFalso dado = new DadoFalso();
        dado.simularValor(3);
        DwarfBardaLonga dwarf = new DwarfBardaLonga("Gimli", dado);
        dwarf.sofrerDano();
        assertEquals( 100.0, dwarf.getVida(), 1e-9 );
    }
}