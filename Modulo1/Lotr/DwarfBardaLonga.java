

public class DwarfBardaLonga extends Dwarf {
    
    private Sorteador sorteador;
    
     public DwarfBardaLonga( String nome) {
        super(nome);
        sorteador = new DadoD6();
    }
    
    public DwarfBardaLonga( String nome, DadoFalso dado ) {
        super(nome);
        sorteador = dado;
    }
  
    @Override 
    public void sofrerDano() {
        boolean devePerderVida = sorteador.sortear() <= 4;
        if (devePerderVida) {
            super.sofrerDano();
        }
    }
}
