

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ElfoVerdeTest
{
    @Test
    public void inventarioItensInicias(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        assertEquals( "Flecha, Arco" , novoElfo.inventario.getDescricoesItens() );
        
    }
    
    @Test
    public void ganhaItensValidos(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        novoElfo.ganharItem(new Item(1, "Flecha de Vidro"));
        novoElfo.ganharItem(new Item(1, "Arco de Vidro"));
        novoElfo.ganharItem(new Item(1, "Espada de Aço Valiriano"));
        assertEquals( "Flecha, Arco, Flecha de Vidro, Arco de Vidro, Espada de Aço Valiriano" , 
        novoElfo.inventario.getDescricoesItens() );
    }
    
    @Test
    public void ganhaItemInvalido(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        novoElfo.ganharItem(new Item(1, "Machado"));
        assertEquals( "Flecha, Arco" , novoElfo.inventario.getDescricoesItens() );
    }
    
    @Test
    public void perdeItemInvalidos(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item arco = new Item(1, "Arco");
        novoElfo.perdeItem(arco);
        Inventario inventario = novoElfo.getInventario();
        assertEquals( "Flecha, Arco" , novoElfo.inventario.getDescricoesItens() );
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
    }
    
    @Test
    public void elfoVerdeGanha2XPorFlecha(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(2, novoElfo.getExperiencia());
    } 
    
    @Test
    public void atirar2FlechasDiminuirZerarFlechas2xAumentarXP(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(4, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
    } 
    
    @Test
    public void atirar3FlechasZerarFlechas(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(4, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
    } 
}
