

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest
{
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXPSofrerDano(){
        ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(3, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());
        assertEquals(85.0 , novoElfo.getVida(), 1e-9);
        assertEquals(Status.SOFREU_DANO, novoElfo.getStatus());
    } 
    
    @Test
    public void atira7FlechaEMorre() {
        ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
        novoElfo.getFlecha().setQuantidade(1000);
        for ( int i = 0 ; i <= 7 ; i++ ){
            novoElfo.atirarFlecha(new Dwarf("Gimli"+ i));
        }
        assertEquals(0.0 , novoElfo.getVida(), 1e-9);
        assertEquals(Status.MORTO, novoElfo.getStatus());
    }
}
    
