

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoDaLuzTest
{
    
    @Test
    public void ElfoDaLuzNasceComEspada(){
        ElfoDaLuz novoElfo = new ElfoDaLuz("Legolas");
        Inventario inventario = novoElfo.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertEquals(new Item(1, "Espada de Galvorn"), inventario.obter(2));
    }
    
    @Test
    public void ElfoDaLuzAtacaComEspadaUmaVez() {
        Dwarf novoDwarf = new Dwarf("Gimli");
        ElfoDaLuz novoElfo = new ElfoDaLuz("Legolas");
        novoElfo.atacarComEspada( novoDwarf );
        assertEquals( 79.0 , novoElfo.getVida(), 1e-9 );
    }
    
    @Test
    public void ElfoDaLuzAtacaComEspadaDuasVezes() {
        Dwarf novoDwarf = new Dwarf("Gimli");
        ElfoDaLuz novoElfo = new ElfoDaLuz("Legolas");
        novoElfo.atacarComEspada( novoDwarf );
        novoElfo.atacarComEspada( novoDwarf );
        assertEquals( 89.0 , novoElfo.getVida(), 1e-9 );
    }

    
}
