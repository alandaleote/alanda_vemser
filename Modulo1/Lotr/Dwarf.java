
public class Dwarf extends Personagem{
    
    
    public Dwarf( String nome ) {
        super(nome);
        this.vida = 110.0;
        this.ganharItem(new Item(1, "Escudo"));
        qtdDanoSofrido = 10.0;
    }
    
    public void equiparEscudo(){
        this.qtdDanoSofrido = 5.0;
    }
    
    public String imprimirResumo(){
        return "Dwarf";
    }
       
    }