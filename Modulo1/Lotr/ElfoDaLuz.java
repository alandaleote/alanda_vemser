import java.util.*;


public class ElfoDaLuz extends Elfo {
    
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    private int contadorAtaque = 0;
    private final double qtdVidaGanha = 10.0;
       
   
    public ElfoDaLuz( String nome)
    {
        super(nome);
        super.ganharItem( new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)) );
        
    }
    
    public boolean devePerderVida(){
        return contadorAtaque % 2 != 0;
    }
    
    public void ganharVida(){
        this.vida += qtdVidaGanha;
    }
    
    @Override
    public void perdeItem( Item item ){
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if (possoPerder){
            super.perdeItem(item);
        }
    }
    
    public void atacarComEspada( Dwarf dwarf ) {
        if( this.getStatus() != Status.MORTO ){
            dwarf.sofrerDano();
            this.aumentarXP();
            contadorAtaque++;
            if(devePerderVida()){
                this.qtdDanoSofrido = 21.0;
                this.sofrerDano();
                this.qtdDanoSofrido = 0.0;
            }
            else{
            this.ganharVida();
            }
            
        }
    }
}


