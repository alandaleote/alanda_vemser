
import java.util.*;

public class EstatisticasInventario
{
    private Inventario inventarioOriginal;
    
    public EstatisticasInventario( Inventario itens){
        //this.itens = itens;
       this.inventarioOriginal = itens;
    }
    
    public double calcularMedia() {
        if (this.inventarioOriginal.getInventario().isEmpty()){
            return Double.NaN;
        }
        double soma = 0;
        for (Item item: this.inventarioOriginal.getInventario() ){
            soma += item.getQuantidade();
        }

        return soma / this.inventarioOriginal.getInventario().size();
    }
    
    public double calcularMediana() {
        this.inventarioOriginal.ordenarItens();
        /*double mediana = 0;
        if ( this.inventarioOriginal.getInventario().size() % 2 != 0 ) {
            int medianaImparIndex = ( this.inventarioOriginal.getInventario().size() + 1 ) / 2;
            Item meio = this.inventarioOriginal.obter(medianaImparIndex);
            mediana = meio.getQuantidade();
        }
        else if ( this.inventarioOriginal.getInventario().size() % 2 == 0 ) {
            int item2Index = ( this.inventarioOriginal.getInventario().size() + 1 ) / 2;
            Item item1 = this.inventarioOriginal.obter(item2Index - 1);
            Item item2 = this.inventarioOriginal.obter(item2Index);
            mediana = ( item1.getQuantidade() + item2.getQuantidade() ) / 2;
        }
        return mediana;*/
        if (this.inventarioOriginal.getInventario().isEmpty()){
            return Double.NaN;
        }
        
        int qtdItens = this.inventarioOriginal.getInventario().size();
        int meio = qtdItens / 2;
        int qtdMeio = this.inventarioOriginal.obter(meio).getQuantidade();
        if( qtdItens % 2 == 1 ){
            return qtdMeio;
        }
        
        int qtdMeioMenosUm = this.inventarioOriginal.obter(meio - 1).getQuantidade();
        return (qtdMeio + qtdMeioMenosUm) / 2;
    }
    
    public int qtdItensAcimaDaMedia() {
        double media = this.calcularMedia();
        int qtdItens = 0;
        for (Item item: this.inventarioOriginal.getInventario() ){
            if ( item.getQuantidade() > media ) {
                qtdItens++;
            }
        }
        
        return qtdItens;
    }
    
}

