
public class Elfo extends Personagem{

    private static int qtdElfos;
    
    public Elfo( String nome ) {
       super(nome);
       this.vida = 100.0;
       this.inventario.adicionar( new Item(2, "Flecha") );
       this.inventario.adicionar( new Item(1, "Arco") ); 
       Elfo.qtdElfos++;
    }
    
    protected void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }
    
    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
   
    public Item getFlecha() {
        return this.inventario.buscar("Flecha");
    }
    
    public int getQtdFlecha() {
        return this.inventario.buscar("Flecha").getQuantidade();  
    }
   
    public boolean podeAtirarFlecha() {
        return this.getQtdFlecha() > 0;
    }
     
    public void atirarFlecha( Dwarf dwarf ) {
        if ( podeAtirarFlecha() ) {
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            this.aumentarXP();
            this.sofrerDano();
            dwarf.sofrerDano();
        }
    }
    
    public String imprimirResumo(){
        return "Elfo";
    }
}
    
