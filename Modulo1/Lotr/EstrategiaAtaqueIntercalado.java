
import java.util.*;

public class EstrategiaAtaqueIntercalado implements EstrategiaDeAtaque{
   
    private ArrayList<Elfo> ordenacao(ArrayList<Elfo> elfos){
        Collections.sort(elfos, new Comparator<Elfo>(){
            public int compare( Elfo elfoAtual, Elfo proximoElfo ){
                boolean TipoDiferente = elfoAtual.getClass() != proximoElfo.getClass();
                
                if (TipoDiferente){
                    return 0;
                }
                
                return elfoAtual instanceof ElfoVerde && proximoElfo instanceof ElfoNoturno ? -1 : 1;
            }
        });
        return elfos;
    }
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        return ordenacao(atacantes);
    }
}
