
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;


public class EstatisticasInventarioTest
{
    @Test
    public void calcularMediaInventarioVazio() {
        Inventario novoInventario = new Inventario();
        EstatisticasInventario inventario = new EstatisticasInventario( novoInventario );
        assertTrue(Double.isNaN(inventario.calcularMedia()));
    }
    
    @Test
    public void calcularMediaApenasUmItem() {
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 2, "Arco" );
        novoInventario.adicionar(item0);
        EstatisticasInventario inventario = new EstatisticasInventario( novoInventario );
        assertEquals( 2, inventario.calcularMedia(), 1e-9);
    }
    
    @Test
    public void calcularMediaComDoisItens() {
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        Item item1 = new Item( 5, "Flecha" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        EstatisticasInventario inventario = new EstatisticasInventario( novoInventario );
        assertEquals( 3, inventario.calcularMedia(), 1e-9);
        
    }
    
    @Test
    public void calcularMediaComTresItens() {
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 2, "Arco" );
        Item item1 = new Item( 5, "Flecha" );
        Item item2 = new Item( 8, "Moeda" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        novoInventario.adicionar(item2);
        EstatisticasInventario inventario = new EstatisticasInventario( novoInventario );
        assertEquals( 5, inventario.calcularMedia(), 1e-9);
        
    }
    
    @Test
    public void QtdItensAcimaDaMedia() {
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        Item item1 = new Item( 5, "Flecha" );
        Item item2 = new Item( 8, "Moeda" );
        Item item3 = new Item( 2, "Adaga" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        novoInventario.adicionar(item2);
        novoInventario.adicionar(item3);
        EstatisticasInventario inventario = new EstatisticasInventario( novoInventario );
        assertEquals( 2, inventario.qtdItensAcimaDaMedia() );
        
    }
    
}
