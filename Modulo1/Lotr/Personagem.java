
public abstract class Personagem {
   protected String nome;
   protected Status status;
   protected Inventario inventario;
   protected double vida, qtdDanoSofrido;
   protected int experiencia, qtdXpPorAtaque;
   
   {
       status = Status.RECEM_CRIADO;
       inventario = new Inventario();
       experiencia = 0;
       qtdXpPorAtaque = 1;
       qtdDanoSofrido = 0;
   }
   
   public Personagem( String nome ){
       this.nome = nome;
    }
   
   public String getNome() {
        return this.nome;
    }
  
   public void setNome( String nome ) {
        this.nome = nome;
    }
    
   public Status getStatus() {
        return this.status;
    }
   
   public Inventario getInventario() {
        return this.inventario;
    }
    
   public double getVida() {
        return this.vida;
    }
    
   public int getExperiencia() {
        return this.experiencia;
    }
    
   public boolean podeSofrerDano() {
        return this.vida > 0;
    }
    
   public void aumentarXP() {
        experiencia = experiencia + qtdXpPorAtaque;    
    }
    
   public void ganharItem( Item item) {
       this.inventario.adicionar(item);
    }
   
   public void perdeItem( Item item) {
       this.inventario.remover(item);
    }
    
   public void sofrerDano() {
        if( this.podeSofrerDano() && this.qtdDanoSofrido > 0.0 ){
            this.vida = this.vida >= this.qtdDanoSofrido ? this.vida - this.qtdDanoSofrido : 0.0;
        }
        
        if( this.vida == 0.0 ) {
            this.status = Status.MORTO;
        }
        else {
            this.status = Status.SOFREU_DANO;
        }
    }
    
   public abstract String imprimirResumo();
  

}
