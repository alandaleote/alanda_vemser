import java.util.*;

public class EstrategiaMaisFlechas implements EstrategiaDeAtaque {
    
    public ArrayList<Elfo> elfosVivosComFlechas(ArrayList<Elfo> elfos){
        ArrayList<Elfo> elfosAptos = new ArrayList<Elfo>();
        for (Elfo elfo : elfos){
            if (elfo.getQtdFlecha() != 0 || elfo.getStatus() != Status.MORTO){
                elfosAptos.add(elfo);
            }
        }
        return elfosAptos;
    }
    
    public ArrayList<Elfo> elfosNoturnosAte30(ArrayList<Elfo> elfosAptos){
        int qntNoturno = 0;
        for (Elfo elfo : elfosAptos){
            if ( elfo instanceof ElfoNoturno ){
                qntNoturno++;
            }
        }
        
        for (Elfo elfo : elfosAptos){
            if ( qntNoturno > elfosAptos.size() * 0.3 && elfo instanceof ElfoNoturno ){
                elfosAptos.remove(elfo);
            }
        }
        return elfosAptos;
    }

    public ArrayList<Elfo> ordenacao(ArrayList<Elfo> elfos){
        elfosNoturnosAte30(elfosVivosComFlechas(elfos));
        Collections.sort(elfos, new Comparator<Elfo>(){
            public int compare( Elfo elfoAtual, Elfo proximoElfo ){
                boolean qtdFlechas = elfoAtual.getQtdFlecha() > proximoElfo.getQtdFlecha();

                return elfoAtual instanceof ElfoVerde || elfoAtual instanceof ElfoNoturno ? -1 : 1;
            }
        });
        return elfos;
    }
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
       return ordenacao(atacantes);
    }
}
