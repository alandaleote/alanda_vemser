

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;


public class InventarioTest{
    
    @Test
    public void criarInventario() {
        Inventario inventario = new Inventario();
        assertNotNull( inventario );
    }
    
    @Test
    public void adicionarUmItem() {
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        novoInventario.adicionar(item0);
        assertEquals( novoInventario.obter(0), item0 );
    }
    
    @Test
    public void obterUmItem() {
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        novoInventario.adicionar(item0);
        assertEquals( novoInventario.obter(0), item0 );
    }
    
    @Test
    public void adicionarTresItens() {
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        Item item1 = new Item( 2, "Flecha" );
        Item item2 = new Item( 1, "Escudo" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        novoInventario.adicionar(item2);
        assertEquals( novoInventario.obter(0), item0 );
        assertEquals( novoInventario.obter(1), item1 );
        assertEquals( novoInventario.obter(2), item2 );
    } 
    
    @Test
    public void removerUmItem() {
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        Item item1 = new Item( 2, "Flecha" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        novoInventario.remover(item0);
        assertNull( novoInventario.buscar( "Arco" ) );
    }
    
    @Test
    public void getItemMaiorQtd(){
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        Item item1 = new Item( 2, "Flecha" );
        Item item2 = new Item( 1, "Escudo" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        novoInventario.adicionar(item2);
        assertEquals( item1 , novoInventario.getItemComMaiorQuantidade() );
    }
    
    @Test
    public void getItemMaiorQtdIguais(){
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        Item item1 = new Item( 2, "Flecha" );
        Item item2 = new Item( 2, "Escudo" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        novoInventario.adicionar(item2);
        assertEquals( item1 , novoInventario.getItemComMaiorQuantidade() );
    }
    
    
    @Test
    public void getDescricoesVariosItens(){
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        Item item1 = new Item( 2, "Flecha" );
        Item item2 = new Item( 1, "Escudo" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        novoInventario.adicionar(item2);
        assertEquals( "Arco, Flecha, Escudo" , novoInventario.getDescricoesItens() );
    }
    
    @Test
    public void getDescricaoNenhumItem(){
        Inventario novoInventario = new Inventario();
        assertEquals( "" , novoInventario.getDescricoesItens() );
    }
    
    @Test
    public void buscarItemDescricao() {
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        Item item1 = new Item( 2, "Flecha" );
        Item item2 = new Item( 1, "Escudo" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        novoInventario.adicionar(item2);
        assertEquals( novoInventario.buscar("Arco")  , item0 );
        assertEquals( novoInventario.buscar("Escudo")  , item2 );
    }
    
    @Test
    public void buscarItemDescricaoIguais() {
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        Item item1 = new Item( 2, "Flecha" );
        Item item2 = new Item( 1, "Arco" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        novoInventario.adicionar(item2);
        assertEquals( novoInventario.buscar("Arco")  , item0 );
    }
    
    @Test
    public void inverterInventarioVazio() {
        Inventario novoInventario = new Inventario();
        assertTrue( novoInventario.inverter().isEmpty() );
    }
    
    @Test
    public void inverterInventario() {
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        Item item1 = new Item( 2, "Flecha" );
        Item item2 = new Item( 1, "Escudo" );
        Item item3 = new Item( 1, "Adaga" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        novoInventario.adicionar(item2);
        novoInventario.adicionar(item3);
        assertEquals( item3 , novoInventario.inverter().get(0) );
        assertEquals( item2 , novoInventario.inverter().get(1) );
        assertEquals( item1 , novoInventario.inverter().get(2) );
        assertEquals( item0 , novoInventario.inverter().get(3) );
    }
    
    @Test
    public void ordenarInventárioQtdAscendente() {
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        Item item1 = new Item( 5, "Flecha" );
        Item item2 = new Item( 7, "Moeda" );
        Item item3 = new Item( 2, "Adaga" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        novoInventario.adicionar(item2);
        novoInventario.adicionar(item3);
        novoInventario.ordenarItens(TipoOrdenacao.ASC);
        assertEquals( item0 , novoInventario.obter(0) );
        assertEquals( item3 , novoInventario.obter(1) );
        assertEquals( item1 , novoInventario.obter(2) );
        assertEquals( item2 , novoInventario.obter(3) );
    }
    
    @Test
    public void ordenarInventárioQtdDescendente() {
        Inventario novoInventario = new Inventario();
        Item item0 = new Item( 1, "Arco" );
        Item item1 = new Item( 5, "Flecha" );
        Item item2 = new Item( 7, "Moeda" );
        Item item3 = new Item( 2, "Adaga" );
        novoInventario.adicionar(item0);
        novoInventario.adicionar(item1);
        novoInventario.adicionar(item2);
        novoInventario.adicionar(item3);
        novoInventario.ordenarItens(TipoOrdenacao.DESC);
        assertEquals( item2 , novoInventario.obter(0) );
        assertEquals( item1 , novoInventario.obter(1) );
        assertEquals( item3 , novoInventario.obter(2) );
        assertEquals( item0 , novoInventario.obter(3) );
    }
}

