
import java.util.*;

public class PaginadorInventario {

   private Inventario inventario;
   private int marcador = 0;

    
   public PaginadorInventario( Inventario itens ){
       //this.itens = itens;
       this.inventario = itens;
   }
    
   public void pular( int inicio ) {
        this.marcador = inicio;     
   }
   
   public int getMarcador() {
       return this.marcador;
   }
  
   public ArrayList<Item> limitar( int qnt ) {
       ArrayList<Item> subConjunto = new ArrayList<>();
       int fim = this.marcador + qnt;
       for ( int i = this.marcador; i < fim && i < this.inventario.getInventario().size(); i++) {
           subConjunto.add(this.inventario.obter(i));
       }
       return subConjunto;
   }
}
   
  